<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * course.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 */

namespace Application\Campus\Controllers;

use Platform;

/**
 * Workspace CRUD action controller for Campus
 *
 * The workspace class implements the action controller that manages the creation,
 * view and edit of workspaces within the campus application.
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
class Course extends Platform\Controller {

    public function __construct() {

        parent::__construct();

        $this->model = $this->load->model('workspace');
        $this->view = $this->load->view('course');

        $_course = $this->input->getVar("course", '');

        if (!empty($_course)):
            $course = $this->model->loadObjectByURI($_course);
            $this->output->setPageTitle($course->getPropertyValue('workspace_name'));

            $course_ = $course->getPropertyData();
            $this->output->set('course', $course_);
        endif;
    }

    public function index() {

        $repository = $this->load->view('repository','system');
        $this->output->setPageTitle(_("Courses"));

        //Get the repository items;
        $this->output->set("repository", array("items"=>array(1,2,3,4,5,6,7,8,9,0)));


        \Library\Event::trigger('beforeCourseRepositoryDisplay', $this);

        return $repository->display(); //if not registered, show false;
    }

    public function create() {
        return $this->edit();
    }

    /**
     * Creates a new course or edits and existing course
     *
     * @param null $courseURI
     */
    public function edit($courseURI = NULL) {


        //Is the user authenticated?
        $this->requireAuthentication();
        //Is the input method submitted via POST;
        $this->output->setPageTitle(_("Create Course"));

        if ($this->input->methodIs("post")) {
            $model = $this->load->model("course");
            //@1 Check where the form is comming from
            //@2 Validate the user permission
            //@3 Privacy settings, If posting to wall can the user post to the wall
            //@4 Add the post;
//            if (!$model->addCourse()) {
//                $this->alert(_("Could not add your post"), null, "error");
//            } else {
//                $courseURI = $model->getLastSavedObjectURI();
//                $courseURL = \Library\Uri::internal("/system/media/timeline/view/$courseURI");
            $courseURL = "#";
            $this->alert(sprintf(_("Course created succesfully. <a href=\"%s\">Add modules, lectures and activities now</a>"), $courseURL), null, "success");
            $this->output->setPageTitle(_("Edit Course"));
//            }
        }

        //form
        //$this->view->editor( "course" );

        $this->view->editor( "course" );
        $this->output->addToPosition("dashboard", $this->output->layout('forms/form', 'system'));

        $this->output->addToPosition("body", $this->output->layout("dashboard", "system"));
    }

    /**
     * Get's an instance of the Workspace controller only creating one if does not
     * exists
     * @staticvar self $instance
     * @return an instance of {@link Workspace}
     *
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}
