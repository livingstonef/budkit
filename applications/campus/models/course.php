<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * course.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 */

namespace Application\Campus\Models;

use Platform;

/**
 * Options management model
 *
 * Manages system options
 *
 * @category  Application
 * @package   Data Model
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 *
 */
class Course extends Platform\Entity {

    public function __construct() {

        parent::__construct();
        //"label"=>"","datatype"=>"","charsize"=>"" , "default"=>"", "index"=>TRUE, "allowempty"=>FALSE
        $this->definePropertyModel(
            array(
                "course_title" => array("Course Title", "mediumtext", 200),
                "course_description" => array("Course Description", "longtext", 2000),
                "course_creator" => array("Course Instructor", "mediumtext", 10),
                "course_start_day" => array("Course Start Day", "varchar", 10),
                "course_start_month" => array("Course Start Month", "varchar", 10),
                "course_start_year" => array("Course Start Year", "varchar", 10),
                "course_end_day" => array("Course End Day", "varchar", 10),
                "course_end_month" => array("Course End Month", "varchar", 10),
                "course_end_year" => array("Course End Year", "varchar", 10),
                "course_privacy" => array("Course Access", "mediumtext", 20),
                "course_taxonomy" => array("Course Category", "mediumtext", 5),
                "course_modules" => array("Course Module Count", "int", 5),
                "course_poster" => array("Course Poster", "mediumtext", 10),
                "course_difficulty" => array("Course Difficulty", "mediumtext", 20),
            ), "course"
        );
        $this->defineValueGroup("course");
        $this->setListOrderBy(array("o.object_updated_on"), "DESC");
    }

    /**
     * Default display method for every model
     * @return boolean false
     */
    public function display() {
        return false;
    }

    public function getAll($active = NULL) {}
    public function getSingle($active = NULL) {}
    /**
     * Adds a new media object to the database
     * @return boolean Returns true on save, or false on failure
     */
    public function addCourse() {

        $inputModel = $this->getPropertyModel();

        foreach ($inputModel as $property => $definition):
            $value = $this->input->getVar($property);
            $this->setPropertyValue($property, $value);
        endforeach;

        //Allow some HTML in media content;
        $summary = $this->input->getFormattedString("course_description", "", "post", true);

        $this->setPropertyValue("course_description", $summary);

        //@TODO determine the user has permission to post;
        $this->setPropertyValue("course_creator", $this->user->get("user_name_id"));

        //If a file has been submitted for profile photo, save that first
        $poster = $this->input->data("files");
        if(!empty($poster['course_poster_image']['size'])):

            //Do we have a file?
            $attachment = $this->load->model("attachment", "system");
            $attachment->setAllowedTypes(array("gif"=>'', "jpeg"=>'', "jpg"=>'', "png"=>''));
            $attachment->setOwnerNameId($this->user->get("user_name_id"));

            $attachment->store($poster['course_poster_image']);

            //Now store the users photo to the database;
            $posterURI = $attachment->getLastSavedObjectURI();
            //Save the attachment
            if (!empty($attachmentURI)) {
                //echo $attachmentURI;
                $this->setPropertyValue("course_poster", $attachmentURI);
            }
        endif;

        //Determine the target
        if (!$this->saveObject(null, "course")) {
            //There is a problem! the error will be in $this->getError();
            return false;
        }
        return true;
    }

    public static function search($query, &$results = array()) {

        $pms = static::getInstance();

        if (!empty($query)):
            $words = explode(' ', $query);
            foreach ($words as $word) {
                $_results =
                    $pms->setListLookUpConditions("message_subject", $word, 'OR')
                        ->setListLookUpConditions("message_body", $word, 'OR');
            }

            $_results = $pms->setListLookUpConditions("message_participants", "(^|,){$pms->user->get('user_name_id')}(,|$)", "AND", FALSE, TRUE, "RLIKE")->getObjectsList("message");
            $rows = $_results->fetchAll();

            $messages = array(
                "filterid" => "messages",
                "title" => "Private Messages",
                "results" => array(),
                "listonly" => true
            );
            //Loop through fetched attachments;
            //@TODO might be a better way of doing this, but just trying
            foreach ($rows as $pm) {
                $limit = 50;
                $string = strip_tags(html_entity_decode(trim($pm['message_body'])));
                $text = explode(" ", $string);
                $continum = (sizeof($text) > $limit) ? " [...]" : NULL;

                $body = (empty($continum)) ? $string : implode(" ", array_splice($text, 0, $limit)) . $continum;
                $message = array(
                    "title" => empty($pm['message_subject']) ? Library\Date\Time::difference(strtotime($pm['object_created_on'])) : $pm['message_subject'], //required
                    "description" => $body, //required
                    "type" => $pm['object_type'],
                    "object_uri" => $pm['object_uri'],
                    "link" => "/system/message/inbox/{$pm['object_uri']}",
                );
                $messages["results"][] = $message;
            }
            //Add the members section to the result array, only if they have items;
            if (!empty($messages["results"]))
                $results[] = $messages;
        endif;

        return true;
    }

    /**
     * Get's an instance of the media model
     * @staticvar object $instance
     * @return object \Application\System\Models\Options
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}

