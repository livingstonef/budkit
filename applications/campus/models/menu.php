<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * menu.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */

namespace Application\Campus\Models;

/**
 * The User EAV model. 
 *
 * @category  Application
 * @package   Data Model
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 * 
 */
class Menu extends \Platform\Model {

    public function __construct() {
        parent::__construct();
    }

    /**
     * Adds Dynamic menu items
     * @param type $menuId
     * @param type $menuItems
     */
    public static function hook(&$menuId, &$menuItems) {

        $user = \Platform\User::getInstance();
        $input = \Library\Input::getInstance();
        $input->getRequestVars();

        //Add the default upload links
        switch ($menuId):

            case "dashboardmenu":
                //Counts
                //$attachments = attachment::getInstance();
                //$mycount = $attachments->setListLookUpConditions("attachment_owner", array($username))->getObjectsListCount("attachment");

                //if (empty($mycount))
                $mycount = NULL;
                $workspace = $input->getVar("workspace", '');
                //Display bookmarks
                $workspace = array(
                    array("menu_title" => "Projects", "menu_url" => "/campus/project/repository", "children" => array(
                            array("menu_title" => "Repository", "menu_url" => "/campus/project/repository"),
                            array("menu_title" => "Tasks", "menu_url" => "/campus/project/tasks"),  //
                            array("menu_title" => "Documents", "menu_url" => "/campus/project/documents"),
                            array("menu_title" => "Add New", "menu_url" => "/campus/project/create")
                        )
                    ),
                    array("menu_title" => "Courses", "menu_url" => "/campus/course/repository", "children" => array(
                            array("menu_title" => "Repository", "menu_url" => "/campus/course/repository"),
                            array("menu_title" => "Enrolled", "menu_url" => "/campus/course/enrolled"),  //
                            array("menu_title" => "Add New", "menu_url" => "/campus/course/create"),
                        )
                    ),
                    array("menu_title" => "Exchange", "menu_url" => "/campus/exchange/h", "children" => array(
                            array("menu_title" => "Repository", "menu_url" => "/campus/exchange/repository"),
                            array("menu_title" => "Bookmarked", "menu_url" => "/campus/exchange/bookmarked"),  //
                            array("menu_title" => "Add New", "menu_url" => "/system/messages/a"),
                        )
                    )
                );
                $menuItems = array_merge($menuItems, $workspace);
                break;

        endswitch;

    }

    /**
     * This model has no data to display
     * @return boolean
     */
    public function display() {
        return false;
    }

    /**
     * Returns an instance of the user EAV model
     * @staticvar object $instance
     * @return object User
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self();
        return $instance;
    }

}

