<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * dashboard.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */

namespace Application\System\Controllers;

/**
 * Start Controller
 *
 * This class implements the actions required for displaying system start pages 
 * including the dashboard and frontpages.
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
class Dashboard extends \Platform\Controller {


    /**
     * Lists all published activities within this timeline;
     * @return void;
     */
    public function index( $display = true) {

        //$_SESSION['somevalue'] = 'This is a value';
        //Get the view;
        $view = $this->load->view('index');
        $user = \Platform\User::getInstance();
        $model = $this->load->model('media');
        $this->output->setPageTitle("Dashboard");
        $activities = $model->setListLookUpConditions("media_target", "")->getAllMedia();
        $model->setPagination(); //Set the pagination vars

        $this->set("activities", $activities);
        //$this->set("user", $user);

        $view->editor();//Generate the forms;
        $timeline = $this->output->layout("timeline");
        //$timelineside = $this->output->layout("timelinenotes");

        \Library\Event::trigger('beforeDashboardDisplay', $this);

        $layout = $this->output->layout('forms/form', 'system');

        $this->output->addToPosition("tray", $layout);
        //$this->load->view('index')->editor();
        //$this->output->set('hidemessagelist', true);
        //$this->output->addToPosition("side", $this->output->layout('forms/form', 'system'));
        $this->output->addToPosition("dashboard", $timeline);
        //$this->output->addToPosition("aside", $timelineside );
        $this->output->addMenuGroupToPosition("dashboardtoolbar", "mediamenu", "nav toolbar-nav toolbar-right", array(), false, false);
        if($display) $view->display(); //sample call;
        //$this->output();
    }

    /**
     * Returns and instantiated Instance of the __CLASS__ class
     * 
     * NOTE: As of PHP5.3 it is vital that you include constructors in your class
     * especially if they are defined under a namespace. A method with the same
     * name as the class is no longer considered to be its constructor
     * 
     * @staticvar object $instance
     * @property-read object $instance To determine if class was previously instantiated
     * @property-write object $instance 
     * @return object __CLASS__
     */
    public static function getInstance() {

        $class = __CLASS__;

        if (is_object(static::$instance) && is_a(static::$instance, $class))
            return static::$instance;

        static::$instance = new $class;

        return static::$instance;
    }
}

