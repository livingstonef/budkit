<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * media.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 */

namespace Application\System\Controllers;
use Library\Event;

/**
 * The parent action controller for all system media types
 *
 * Defines common action methods for the managing the default system media types
 * It is important that you inherit the key features defined in this class
 * when defining media types
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 *
 */
Class Media extends \Platform\Controller {

    public function index() {return false;}

    public function edit ($itemURI = null, $type="media") {

        $this->view = $this->load->view('index');
        $this->view->editor( "editor" );

        $type = $this->input->getVar("type", "", $type);

        //Editors for custom types; exit at end of call;
        Event::trigger("beforeMediaEdit", $itemURI, $type);

        //Another strategy when using custom type editors, is to add $itemURI = null; at end of editor method;
        //If using custom media editor hook, remember to add afterMediaEdit event to your editor method;
        if(empty($itemURI)) return false;

        $model = $this->load->model($type, "system");
        $_media = $model->getMedia($type, $itemURI);
        //static::canAccess( $itemURI );

        $media = reset($_media['items']);
        $this->set("media", $media);

        $this->output->setPageTitle(_("Editing...{$media['title']}"));

        $layout = $this->output->layout('forms/form', 'system');

        $this->output->addToPosition("body", $layout);
        //$this->output->addToPosition("aside", $this->output->layout('messages/chat/presence'));

        //$this->view->display(); //sample call;
        //$this->output->addToPosition("right", $right );
    }



    public function create($form = NULL) {

        $this->output->setPageTitle(_("Participate"));
        $this->view = $this->load->view('index');

        $this->view->editor( $form );
        $layout = $this->output->layout('forms/form', 'system');

        $this->output->addToPosition("body", $layout);
        //$this->output->addToPosition("aside", $this->output->layout('messages/chat/presence'));

        //$this->view->display(); //sample call;
        //$this->output->addToPosition("right", $right );

    }

    /**
     * Deletes an media from the timeline;
     * @return Media::delete();
     */
    public function delete( $itemUri = NULL ) {

        //Get the model
        $model =  $this->load->model('media', 'system');
        if(!$model->removeObject($itemUri)):
            $this->alert(_("Could not delete your post id:{$itemUri}."), $this->getError(), "error");
        endif;
        //Notify the user;
        $this->alert("The item has now been removed","","info");

        return $this->returnRequest();

    }


    public function timeline() {
        $timeline = $this->load->controller("media\\timeline", "system");
        return $timeline->index();
    }

    /**
     * Displays an collection media.
     * @todo    Implement the collection read action method
     * @return  void
     */
    public function view($mediaObjectURI = null) {
        $timeline = $this->load->controller("media\\timeline", "system");

        return $timeline->view($mediaObjectURI,"attachment");
    }

    /**
     * Returns an instance of the media controller
     * @staticvar self $instance
     * @return Media
     */
    public static function getInstance() {

        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}

