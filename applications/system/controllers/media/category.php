<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * category.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */
namespace Application\System\Controllers\Media;

use Application\System\Controllers\Media;

/**
 * Article CRUD action controller for system media 
 *
 * This class implements the action controller that manages the creation, 
 * view and edit of articles.
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
final class Category extends Media {


    public function create(){


        \Library\Event::register("beforeEditorDisplay", function(&$formlist){
            $formlist = array_merge($formlist, array(
                array("id"=>"taxonomy","title"=>"Add Category","layout"=>"forms/taxonomy","app"=>"system","icon-class"=>"icon-fire","hint"=>"Create Category"),
            ));
        });

        $this->load->view("index")->editor("taxonomy");


        $form = $this->output->layout("forms/form", "system");
        $this->output->addToPosition("dashboard", $form);


        $this->load->view("index")->display();
    }

    /**
     * Displays a gallery of media items. 
     * @return void
     */
    public function gallery() {

        $this->output->setPageTitle(_("Categories"));
        $this->output->set("action", array(
            "button" => array(
                "link" => "/system/media/category/create",
                "text" => "Add Category",
                "class" =>"btn-important span12"
            )
        ));
        $model = $this->load->model("attachment", "system");
        $text = $this->config->getParam( "text", array(), "attachments");
        
        $attachments = $model
                ->setListLookUpConditions("attachment_owner", array($this->user->get("user_name_id")))
                ->setListLookUpConditions("attachment_type", $text) //Limits the lookup to attachments with image types
                ->setListOrderBy("o.object_created_on", "DESC")
                ->getObjectsList("attachment");
        $model->setPagination(); //Set the pagination vars
        
        $items = array("totalItems" => 0);
        //Loop through fetched attachments;
        //@TODO might be a better way of doing this, but just trying
        while ($row = $attachments->fetchAssoc()) {
            $row['attachment_url'] = "/system/object/{$row['object_uri']}";
            $items["items"][] = $row;
            $items["totalItems"]++;
        }
        if ((int)$items["totalItems"] > 0)
            $this->set("gallery", $items);

        //Get the repository items;
        $this->output->set("repository", array("items"=>array(1,2,3,4,5,6,7,8,9,0)));

        $gallery = $this->output->layout("media/repository");
        $this->output->addToPosition("dashboard", $gallery);


        $this->load->view("repository","system")->display();
    }
    /**
     * Returns an instance of the article controller, only creating one if does not
     * exists
     * @staticvar self $instance
     * @return an instance of {@link Article}
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }
}
