<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * message.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */

namespace Application\System\Controllers\Message;

use Application\System\Controllers;
use Platform\Protocol\Ws;

/**
 * Messages CRUD action controller
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
class Chat extends Controllers\Message {

    public function index(){

        $this->view = $this->load->view("message");
        $this->output->setPageTitle('Live');
        $this->output->set("repository", array("items"=>array(1,2,3,4)));
        $this->output->addToPosition("dashboard", $this->output->layout('media/repository'));

        $this->view->display();
    }


    /**
     * Messages index action
     * @return void
     */
    public function live(){

        $server =  FSPATH . 'server.php';
        $start  = "exec nohup php {$server} > /dev/null 2>&1 &";
        exec('bash -c "'.$start.'"');

        $this->view = $this->load->view("message");

        $this->view->editor(array("id"=>"chat","title"=>"Chat","layout"=>"forms/chat","app"=>"system","icon-class"=>"icon-chat","mediaverb"=>"chat", "mediatarget"=>'chatid') );

        $this->output->addToPosition("side", $this->output->layout("messages/chat"));
        $this->output->addToPosition("body", $this->output->layout('messages/chat/board'));
        $this->output->addToPosition("aside", $this->output->layout('messages/chat/presence'));
        
        $this->view->display();
    }



    public function stop(){}

    /**
     * Returns an instsance of the message action controller
     * @staticvar object $instance
     * @return object Message
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}
