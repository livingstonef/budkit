<?php

namespace Application\System\Controllers\Message\Chat;

use Application\System\Controllers\Message;
use Platform\Protocol\Ws;

/**
 * Example application for ws: echo server
 */
class EchoApplication extends Ws\Application
{
    /**
     * @see Wrench\Application.Application::onData()
     */
    public function onData($data, $client)
    {
        $client->send($data);
    }
}