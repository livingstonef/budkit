<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * application.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */

namespace Application\System\Controllers\Message\Chat;

use Application\System\Controllers\Message;
use Platform\Protocol\Ws;

/**
 * Messages CRUD action controller
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
final class Application extends Ws\Application{

    private $connections           = array();


    public function onConnect($connection){
        $id = $connection->getId();
        $this->connections[$id] = $connection;
    }

    public function onDisconnect($connection){
        $id = $connection->getId();
        unset($this->connections[$id]);
    }

    /**
     * @see Wrench\Application.Application::onData()
     */
    public function onData($payload, $connection){

        if(count($this->connections) > 1):
            foreach($this->connections as $participant):
                $participant->send($payload);
            endforeach;
        else:
            $connection->send($payload);
        endif;

    }
}
