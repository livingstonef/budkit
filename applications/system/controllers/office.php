<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * office.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 * 
 */

namespace Application\System\Controllers;

use Platform;

/**
 * Office CRUD action controller for Campus 
 *
 * The office class implements the action controller that manages the creation, 
 * view and edit of offices within the campus application.
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
class Office extends Platform\Controller {

    public function __construct() {
        parent::__construct();

        $this->model = $this->load->model('office');
        $this->view = $this->load->view('office');

        $_office = $this->input->getVar("office", '');
        
        if (!empty($_office)):
            $office = $this->model->loadObjectByURI($_office);
            $this->output->setPageTitle($office->getPropertyValue('office_name'));

            $office_ = $office->getPropertyData();
            $this->output->set('office', $office_);
        endif;
    }

    public function index(){}


    /**
     * Creates a new office
     * @return void
     */
    public function create() {

        $this->view = $this->load->view("office");
        $this->model = $this->load->model("office");

        $this->output->setPageTitle(_("New Office"));

        //Save New office;
        if ($this->input->methodIs("post")) {

            //If a file has been submitted for profile photo, save that first
            $attachmentfile = $this->input->data("files");
            if (!empty($attachmentfile['office_coverphoto']['size'])):
                //Do we have a file?
                $attachment = $this->load->model("attachment", "system");
                $attachment->setAllowedTypes(array("gif" => '', "jpeg" => '', "jpg" => '', "png" => ''));
                $attachment->setOwnerNameId($this->user->get("user_name_id"));

                $attachment->store($attachmentfile['office_coverphoto']);
                //Now store the worrkspace photo to the database;
                $attachmentUri = $attachment->getLastSavedObjectURI();
            endif;

            //validate office info;
            $name = $this->input->getString('office_name');
            $privacy = $this->input->getString('office_privacy');
            $shortDescr = $this->input->getString('office_short_descr');
            $longDescr = $this->input->getFormattedString("office_long_descr", "", "post", true);

            if (empty($name) || empty($privacy) || empty($shortDescr)):
                $this->alert("Incomplete office data recieved, Please complete the required fields(*)", '', 'error');
                $this->redirect($this->input->getReferer());
                return false; //useless
            endif;

            $this->model->setPropertyValue("office_creator", $this->user->get('user_name_id'));
            $this->model->setPropertyValue("office_name", $name);
            $this->model->setPropertyValue("office_privacy", $privacy);
            $this->model->setPropertyValue("office_short_descr", $shortDescr);

            if (!empty($attachmentUri))
                $this->model->setPropertyValue("office_cover_photo", $attachmentUri);

            //set the data;
            if (!empty($longDescr))
                $this->model->setPropertyValue("office_long_descr", $longDescr);

            //Save the message
            if (!$this->model->saveObject(null, "office")) {
                return $this->returnRequest("The office could not be created, an error occured", "error");
            }
            //Now store the worrkspace photo to the database;
            $officeUri = $this->model->getLastSavedObjectURI();
            if (!empty($officeUri)) {
                $this->alert("Welcome to your new office", '', 'success');
                $officeUrl = "/campus/office:{$officeUri}/office/overview/";
                $this->redirect($officeUrl);
                return true; //useless
            }
        }

        $this->view->editor(
            array("id" => "office", "title" => "Status", "layout" => "forms/office", "icon-class" => "icon-lightbulb")
        );
        $layout = $this->output->layout('forms/form', 'system');
        $this->output->addToPosition("dashboard", $layout);

        $this->view->directory();
    }

    /**
     * Get's an instance of the Office controller only creating one if does not
     * exists
     * @staticvar self $instance
     * @return an instance of {@link Office}
     * 
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}
