<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * calendar.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 */
namespace Application\System\Controllers\Office;

use Application\System\Controllers as System;


/**
 * Calendar CRUD action controller.
 *
 * This class implements the action controller that manages the creation,
 * view and edit of calendars.
 *
 * @category  Application
 * @package   Action Controller
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 */
final class Event extends System\Office {


    public function create() {
        $this->output->setPageTitle(_("Create Event"));
        $this->view = $this->load->view('index');

        $this->view->editor(  array("id"=>"event","title"=>"Create Event","layout"=>"forms/event","app"=>"system","icon-class"=>"icon-calendar","hint"=>"Add an event to your calendar") );
        $layout = $this->output->layout('forms/form', 'system');

        $this->output->addToPosition("dashboard", $layout);

        $this->view->display();
    }

    public function update() {}
    public function edit(){}
    public function index() {return true;}
    public function delete() {}


    /**
     * Returns and instance of the event class
     * @return System\an|Event
     */
    public static function getInstance() {

        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }

}
