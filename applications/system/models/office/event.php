<?php

/* vim: set expandtab tabstop=4 shiftwidth=4 softtabstop=4: */

/**
 * event.php
 *
 * Requires PHP version 5.4
 *
 * LICENSE: This source file is subject to version 3.01 of the GNU/GPL License
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 */

namespace Application\System\Models\Office;

use Application\System\Models\Media;
use Platform;

/**
 * Attachment management model
 *
 * All attachments are saved as objects in EAV database
 *
 * @category  Application
 * @package   Data Model
 * @license   http://www.gnu.org/licenses/gpl.txt.  GNU GPL License 3.01
 * @version   1.0.0
 * @since     Jan 14, 2012 4:54:37 PM
 * @author    Livingstone Fultang <livingstone.fultang@stonyhillshq.com>
 *
 */
final class Event extends Media{


    /**
     * Defines the attachment properties
     *
     * @return void
     */
    public function __construct() {

        parent::__construct();
        /**
         * The event model extends the media model, use the core media entity properties
         * like media_title etc for event title, media_actor is the creator of the event
         * and should be it's administrator. set media type to event!
         * @TODO should we add pages for each individual event?
         */
        $this->extendPropertyModel(
            array(
                "event_start_timestamp" => array("Event Start", "mediumtext", 50),
                "event_stop_timestamp" => array("Event Stop", "mediumtext", 50),
                "attachment_type" => array("Attachment Content Type", "mediumtext", 100)
            ), "event"
        );
    }

    /**
     * Default display method for every model
     * @return boolean false
     */
    public function display() {
        return false;
    }

    /**
     * Searches the database for attachments
     *
     * @param type $query
     * @param type $results
     */
    public static function search($query, &$results = array()) {

        return true;
    }


    /**
     * Get's an instance of the media model
     * @staticvar object $instance
     * @return object \Application\System\Models\Options
     */
    public static function getInstance() {
        static $instance;
        //If the class was already instantiated, just return it
        if (isset($instance))
            return $instance;
        $instance = new self;
        return $instance;
    }
}

