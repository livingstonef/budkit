; the design configuration file
; copy and rename this file to design.ini
; remember there must be a '/' at the end of your theme name!
[design]
    theme    = themes/sample-theme/;
; you should add a separate ini file for each config group
; so create a file called appearance.ini and add the following lines
[appearance]
    style    = styles/sample-style/;
