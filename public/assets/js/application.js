/**
 * Created by livingstonefultang on 10/27/13.
 */
require(["jquery","mes","jquery.cookie","jquery.chosen","jquery.bootstrap","jquery.budkit"], function() {
    var activeTabs = {};
    if($.cookie("activeTabs")){
        activeTabs = JSON.parse($.cookie("activeTabs"));
        $.each(activeTabs, function(index, el){
            // console.log(index);
            $(index).removeClass().addClass(el.className);
           // $(window).trigger('resize');
        });
    }
    $('[data-remember-event]').each(function(index, el){
        var elEvent = $(el).data('remember-event'),
            elId    = $(el).attr('id');
        $(el).on(elEvent, function(e) {
            //alert('toggled');
            activeTabs['#'+elId] = {className:$(el).attr('class')};
            $.cookie("activeTabs", JSON.stringify(activeTabs, null, 2) , {
                expires: 90,
                path: '/'
            } );
        });
    });

    $("select:not(select.native)").chosen({disable_search_threshold: 10,allow_single_deselect: true})

    //the jquery.alpha.js and jquery.beta.js plugins have been loaded.
    $(function(){
        var $window = $(window);
        var $body   = $(document.body);
        $("[data-toggle]").on('click', function(e){
            e.preventDefault();
            var  target = $(this).attr('data-target'),
                detail = $(this).attr('data-toggle');

            $(target).toggleClass(detail);
            $(target).trigger('toggle');
            $(window).trigger('resize');
        })
    });

    $('[data-target="media-player"]').each(function(){
//            var msource = $(this).attr("data-source"),
//                mformat = $(this).attr("data-formats"),
//                mstyler = $(this).attr("data-ancestor"),
//                media   = {};
        var $templatePath = $(this).next('[name="pluginpath"]').attr("value"),
            $figure       = $(this).parent('figure')
            ;

        $(this).mediaelementplayer({
            alwaysShowControls: false,
            preload:true,
            plugins: ['flash','silverlight'],
            pluginPath: $templatePath+'/assets/js/mes/',
            pluginWidth: $figure.width(),
            flashName: 'flashmediaelement.swf',
            silverlightName: 'silverlightmediaelement.xap',
            features: ['playpause','current','progress','duration','tracks','volume','fullscreen'],
            // method that fires when the Flash or Silverlight object is ready
        });
    })
});
