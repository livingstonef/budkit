/* ===================================================
 * budkit-editor.js v0.0.1
 * http://budkit.org/docs/editor
 * ===================================================
 * Copyright 2012 The BudKit Team
 *
 * This source file is subject to version 3.01 of the GNU/GPL License 
 * that is available through the world-wide-web at the following URI:
 * http://www.gnu.org/licenses/gpl.txt  If you did not receive a copy of
 * the GPL License and are unable to obtain it through the web, please
 * send a note to support@stonyhillshq.com so we can mail you a copy immediately.
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * ========================================================== */
!function($) {
    "use strict"
    //Class Definition
    var BKEditor = function(element, options) {

        var $editor = this;

        this.selection = window.getSelection();
        this.range = 0;

        this.options    = $.extend({}, $.fn.bkeditor.defaults, options)
        this.element    = $(element);
        this.wrapper    = this.element.closest( this.options.editorwrap );

        this.editorbar  = $("<div/>").hide().addClass("bkeditor-toolbar btn-toolbar").appendTo( this.wrapper.find( this.options.editortoolbar ) );


        //are we showing the toolbar?
        if (typeof this.element.attr("toolbar") !== 'undefined' && this.element.attr("toolbar") !== false) {
            this.options.hidetoolbar = false;
        }

        this.editor =  $("<div />");

        //this.copyAttributes(["spellcheck", "value", "placeholder"]).from( this.element ).to(this.editor);
        $(this.editor).attr("contenteditable", true)
            .html(this.element.val())
            .addClass("bkeditor-content input form-control")
            .attr("id", $(element).attr('id'))

            .appendTo( this.wrapper.find( this.options.editorcontainer ) )

            .on('click mousedown keyup keydown blur', $.proxy(function(e) {
               // console.log(this.editor.width());
                this.updateElement()}, this)
            )
            .on('focus', function(){
                var s = window.getSelection();
                var t = 'savedRange'+$(this).attr('id');
                if (window[t] != null && s.rangeCount > 0)
                    s.removeAllRanges();
                s.addRange(window[t]);
            }).bind("mouseup keyup",function(){
                var t = 'savedRange'+$(this).attr('id');
                window[t] = window.getSelection().getRangeAt(0);
            }).bind("mousedown click",function(e){
                if(!$(this).is(":focus")){
                    e.stopPropagation();
                    e.preventDefault();
                    $(this).focus();
                }
            });

        //Not all browsers use ContentWindow this will fail in Firefox
        var edit = this.editor;
        edit.designMode = 'on';

        this.element.data('editor', this.editor);

        this.togglePlaceHolder();

        //Determining the height of the editor is not  straightforward with hidden editors
        //Not the best solution but it works for now
        var clonedEditor = this.element.clone();
        $("body").append(clonedEditor);
        var editorHeight = clonedEditor.height();
        //editorScrollHeight = clonedEditor.prop('scrollHeight');
        //editorWidth  = clonedEditor.outerWidth();
        clonedEditor.remove();

        //console.log(editorScrollHeight);
        this.editor.height(editorHeight).attr("width", "100%");
        this.element.hide();

        //Initialize the toolbar
        this.toolbar = new BKEditorToolbar(this, this.options);
    }
    BKEditor.prototype = {
        Constructor: BKEditor,
        toString: function(html) {
            html = html || false;
            return (!html) ? $(this.editor).text() : $(this.editor).html();
        },
        updateElement: function() {
            //this.toolbar.build();
            //console.log("Element:::: scrollheight: "+$(this.editor).outerHeight()+", iframeheight: "+ this.iframe.height() );
            this.element.val(this.toString(true));
            //this.updateEditor()
            //console.log($(this.element).prop('scrollHeight'));
        },
        updateEditor: function() {
            //need to update the editor closest to the contenteditable element being edited
            //so find the editor that is within the wrapper.
            $(this.wrapper.find(this.editor) ).html(this.element.val());
        },
        copyAttributes: function(attributesToCopy) {
            return {
                from: function(elementToCopyFrom) {
                    return {
                        to: function(elementToCopyTo) {
                            var attribute, i = 0, length = attributesToCopy.length;
                            for (; i < length; i++) {
                                attribute = attributesToCopy[i];
                                if (typeof(elementToCopyFrom.attr(attribute)) !== "undefined" && elementToCopyFrom.attr(attribute) !== "") {
                                    elementToCopyTo.attr(attribute, elementToCopyFrom.attr(attribute));
                                }
                            }
                        }
                    };
                }
            };
        },
        browserIsMsie: function() {
            return /msie/.test(navigator.userAgent.toLowerCase());
        },
        formatBlock: function(value) {
            this.ec("formatblock", false, value || null);
        },
        togglePlaceHolder: function() {
            var CLASS_NAME = "placeholder",
                $this = this,
                $editor = $(this.editor),
                unset = function() {
                    if ($editor.hasClass(CLASS_NAME)) {
                        $editor.empty();
                    }
                    $editor.removeClass(CLASS_NAME);
                },
                set = function() {
                    if ($editor.is(":empty")) {
                        $editor.html($this.element.attr("placeholder"));
                        $editor.addClass(CLASS_NAME);
                    }
                };
            $editor.on("focus click keyup keydown mousedown", unset)
            $($editor).blur(set);
            set();
        },
        execCommand: function(a, b, c) {
            $(this.editor).trigger('focus').focus();
            //window.getSelection().setPosition(0);
            document.execCommand(a, b || false, c || null);
            this.updateElement();
        },
        ec: function(a, b, c) {
            this.execCommand(a, b, c);
        },
        queryCommandValue: function(a) {
            $(this.editor).trigger('focus').focus();
            return this.editor.queryCommandValue(a);
        },
        qc: function(a) {
            return this.queryCommandValue(a);
        }
    };
    var BKEditorToolbar = function($this, options) {
        var $toolbar = this,
            $customtools = $this.element.data('toolbar-tools'); //using custom tools

        this.options = $.extend({}, options, ($customtools)?{toolbar:$customtools}:{});

        if (this.options.hidetoolbar)
            return $toolbar;
        //console.log(this.options.toolbar);


        $.each(this.options.toolbar, function(i, commands) {
            if ($.isArray(commands)) {
                //Then we are defining a custom object 
                //or it is a lonely item;
                $toolbar.getBtnGroup(commands, $this).appendTo($this.editorbar);
            }else if($.isPlainObject(commands)){
                //dropdowlist;
                //console.log(commands);
                $toolbar.getBtnGroupDropdownList(commands, $this);
            }
        });
        //Show toolbar on focus, hide onexit;
        //console.log( $this.editor.html());
        if (this.options.showtoolbaronedit) {
            $($this.editor).on("click focus keydown keyup mousedown", function() {
                $this.editorbar.show();
            });
        }
        else {
            $this.editorbar.show();
        }
    };
    BKEditorToolbar.prototype = {
        commands: {
            columns: ['Grid', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            button: ['Button', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            alert: ['Alert', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            collapsible: ['Collapsible', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            panel: ['Panel', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            slideshow: ['Slideshow', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            //class, title, onClick, onAfterExecute,
            bold: ['bold', '', function($this, btn) {
                //Todo we need to check the selected text;
                $(btn).toggleClass("active");
                $this.ec("bold")
            }],
            italic: ['italic', '', function($this, btn) {
                $(btn).toggleClass("active");
                $this.ec("italic")
            }],
            underline: ['underline', '', function($this, btn) {
                $(btn).toggleClass("active");
                $this.ec("underline");
            }],
            strikethrough: ['strikethrough', '', function($this, btn) {
                $(btn).toggleClass("active");
                $this.ec("strikethrough");
            }],
            orderedlist: ['list-ol', '', function($this) {
                $this.ec("insertorderedlist");
            }],
            unorderedlist: ['list-ul', '', function($this) {
                $this.ec("insertunorderedlist");
            }],
            horizontalrule: ['Horizontal Line', '', function($this) {
                $this.ec("insertHorizontalRule", false);
            }],
            outdent: ['outdent', '', function($this) {
                $this.ec("outdent");
            }],
            indent: ['indent', '', function($this) {
                $this.ec("indent");
            }],
            undo: ['undo', '', function($this) {
                $this.ec("undo");
            }],
            redo: ['repeat', '', function($this) {
                $this.ec("redo");
            }],
            paragraph: ['paragraph', 'P', function($this) {
                $this.formatBlock("<p>")
            }],
            link: ['link', '', function($this) {
                if ($this.browserIsMsie()) {
                    $this.ec("createLink", true);
                } else {
                    $this.ec("createLink", false, prompt("URL:", "http://"));
                }
            }],
            unlink: ['unlink', '', function($this) {
                $this.ec("unlink", false, []);
            }],
            image: ['picture-o', '', function($this) {
                if ($this.browserIsMsie()) {
                    $this.ec("insertImage", true);
                } else {
                    $this.ec("insertImage", false, prompt("URL:", "http://"));
                }
            }],
            video: ['film', '', function($this) {
                if ($this.browserIsMsie()) {
                    $this.ec("insertImage", true);
                } else {
                    $this.ec("insertImage", false, prompt("URL:", "http://"));
                }
            }],
            upload: ['cloud', '', function($this, btn) {
                //1. load the bkmodal;
                $(btn).bkmodal({
                    title: 'Insert Media',
                    action: $this.element.data('upload-action')
                });
            }],
            markup: ['code', '', function($this) {
                if ($this.browserIsMsie()) {
                    $this.ec("insertImage", true);
                } else {
                    $this.ec("insertImage", false, prompt("URL:", "http://"));
                }
            }],
            h1: ['h1', 'H1', function($this) {
                $this.formatBlock($this.browserIsMsie() ? "Heading 1" : "h1");
            }],
            h2: ['h2', 'H2', function($this) {
                $this.formatBlock($this.browserIsMsie() ? "Heading 2" : "h2");
            }],
            h3: ['h3', 'H3', function($this) {
                $this.formatBlock($this.browserIsMsie() ? "Heading 3" : "h3");
            }],
            h4: ['h4', 'H4', function($this) {
                $this.formatBlock($this.browserIsMsie() ? "Heading 4" : "h4");
            }],
            fullscreen: ['fullscreen', '', function($this, btn) {
                var fsc = $this.options.iconprefix + '-fullscreen',
                    unfsc= $this.options.iconprefix + '-unfullscreen';
                $this.element.parents(".bkeditor-holder").toggleClass("fullscreen").trigger('toggle');
                $this.element.parents(".bkeditor-holder-container").toggleClass("container").trigger('toggle');
                $(btn).toggleClass("active").find('i').toggleClass(function() {
                    if ( $( this ).is( "."+fsc ) ) {
                        return unfsc;
                    } else {
                        return fsc;
                    }
                });
            }]
        },
        getBtnGroupDropdownList: function(group, $this) {
            var $toolbar = this;
            //console.log(group);
            $.each(group, function(i, commands) {
                var btnGroup = $("<div/>").addClass("btn-group"),
                    btn = $("<a/>").addClass("btn btn-default dropdown-toggle").attr("data-toggle","dropdown").text(i),
                    btnicon  = $("<i/>").addClass($this.options.iconprefix),
                    btncaret = $("<span/>").addClass('caret'),
                    cmdList  = $('<ul/>').addClass('dropdown-menu').appendTo(btnGroup);
                //console.log(i);
                btnicon.addClass($this.options.iconprefix + '-'+ i).prependTo(btn);
                btncaret.appendTo(btn)
                btn.appendTo(btnGroup);
                $.each(commands, function(i, command) {

                    if(!command) return $('<li/>').addClass('divider').appendTo(cmdList);

                    var cmdLi = $('<li/>'),
                        cmdA  = $('<a/>'),
                        cmdIcon = $("<i/>").addClass($this.options.iconprefix),
                        cmdObj = (typeof command == "string" && $toolbar.commands[command]) ? $toolbar.commands[command] : command;

                    ;
                    if (typeof cmdObj== "function") {
                        cmdObj($this, $toolbar, btnGroup);
                    }else{
                        if ($.isArray(cmdObj)) {
                            cmdIcon.addClass($this.options.iconprefix + '-'+ cmdObj[0]).appendTo(cmdA);
                            if (cmdObj[0] && typeof cmdObj[0] == "string") {
                                cmdA.append(cmdObj[0]);
                            }
                            //console.log(btnObj);
                            //Add Button method;
                            if (cmdObj[2] && typeof cmdObj[2] == "function") {
                                cmdA.on('click', function() {
                                    cmdObj[2]($this, this);
                                });
                            }
                            cmdA.appendTo(cmdLi);
                            cmdLi.appendTo(cmdList);
                        }
                    }
                });
                btnGroup.appendTo($this.editorbar);
                //return btnGroup;
            });
        },
        getBtnGroup: function(group, $this) {
            var $toolbar = this,
                btnGroup = $("<div/>").addClass("btn-group");
            if (!$.isArray(group))
                return $();
            $.each(group, function(i, btn) {
                $toolbar.getBtn(btn, $this).appendTo(btnGroup);
            });

            return btnGroup;
        },
        getBtn: function(button, $this) {
            var btn = $("<a/>").addClass("btn btn-default"),
                btnicon = $("<i/>").addClass(this.options.iconprefix),
                commands = this.commands,
                btnObj = (typeof button == "string" && commands[button]) ? commands[button] : button;
            if (typeof btnObj== "function") {
                btnObj($this, this, btn);
                return btn;
            }else{
                if ($.isArray(btnObj)) {
                    btnicon.addClass(this.options.iconprefix + '-'+ btnObj[0]).appendTo(btn);
                    if (btnObj[1] && typeof btnObj[1] == "string") {
                        btn.append(btnObj[1]);
                    }
                    //console.log(btnObj);
                    //Add Button method;
                    if (btnObj[2] && typeof btnObj[2] == "function") {
                        btn.on('click', function() {
                            btnObj[2]($this, this);
                        }).addClass('btn-'+btnObj[0]);
                    }
                    return btn;
                }
            }
            return $();
        },
        toggleBtnActive: function() {
        },
        toggleBtnDisable: function() {
        },
        inactivateAllBtn: function() {
        }
    }
    //Plugin Defintion
    $.fn.bkeditor = function(option) {
        return this.each(function() {
            var $this = $(this)
                , data = $this.data('bkeditor')
                , options = typeof option == 'object' && option
            ;
            if (!data)
                $this.data('bkeditor', (data = new BKEditor(this, options)));
        });
    };
    $.fn.bkeditor.defaults = {
        iconprefix: 'fa',
        hidetoolbar: true,
        showtoolbaronedit: false,
        editorwrap: '.panel-input',
        editortoolbar : '.panel-heading-toolbar',
        editorcontainer: '.panel-editor-container',
        toolbar:[["bold", "italic", "underline", "strikethrough"],["link", "unlink", "image"]]
    };
    $.fn.bkeditor.Constructor = BKEditor;
    /* EDITOR DATA-API
     * ============== */

    $(function() {
        $('[data-target="budkit-editor"]').bkeditor();
    })
}(window.jQuery);
