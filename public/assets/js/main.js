/**
 * Created by livingstonefultang on 19/12/2013.
 */

// Require JS Configuration
requirejs.config({
    "paths": {
        "masonry": "libs/masonry/masonry.pkgd.min",
        "jquery" : "libs/jquery/jquery-1.10.2.min",
        "mes" : "libs/mes/mediaelement-and-player.min",
        "ace" : "//cdnjs.cloudflare.com/ajax/libs/ace/1.1.01/ace",
        "jquery.cookie" : "libs/cookie/jquery.cookie",
        "jquery.chosen" : "libs/chosen/chosen.jquery.min",
        "jquery.bridget": "libs/bridget/jquery.bridget",
        "jquery.fullcalendar": "libs/fullcalendar/fullcalendar",
        "jquery.bootstrap": "libs/bootstrap/bootstrap.min",
        "jquery.budkit": "libs/budkit/budkit",
        "jquery.validate": "libs/validate/jquery.validate.min",
        "budkit.uploader" : "libs/budkit/budkit-uploader",
        "budkit.chat" : "libs/budkit/budkit-chat",
        "budkit.editor" : "libs/budkit/budkit-editor",
        "budkit.modal" : "libs/budkit/budkit-modal"
    },
    shim: {
        'jquery': {
            exports: '$'
        },
        'mes' : ['jquery'],
        'jquery.chosen':    ['jquery'],
        'jquery.cookie': ['jquery'],
        'jquery.bridget': ['jquery'],
        'jquery.fullcalendar': ['jquery'],
        'jquery.bootstrap': ['jquery'],
        'jquery.budkit': ['jquery'],
        'jquery.validate': ['jquery'],
        'budkit.uploader': ['jquery'],
        'budkit.editor': ['jquery'],
        'budkit.modal': ['jquery'],
        'budkit.chat': ['jquery']
    }
});

// Start the application
requirejs(["application"]);
