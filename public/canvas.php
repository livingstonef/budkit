<!DOCTYPE html>
<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <html class="no-js" lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="${page.description}" />
        <meta name="author" content="${page.author}" />
        <meta name="keywords" content="${page.author}" />
        <link rel="shortcut icon" href="${config|design.theme}/assets/ico/favicon.png" />
        <title><tpl:element type="text" data="page.title">Budkit</tpl:element></title>
        <!-- Maverick core CSS -->
        <link href="${config|design.theme}/assets/css/bootstrap.css" rel="stylesheet" />
        <!--        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css' />-->
        <!--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>-->
        <script data-main="${config|general.path}${config|design.theme}assets/js/main" src="${config|design.theme}/assets/js/require.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="${config|design.theme}/assets/js/html5shiv.js"></script>
        <script src="${config|design.theme}/assets/js/respond.min.js"></script>
        <![endif]-->
        <tpl:utility type="head" />
    </head>
    <body class="background-clouds">
    <div class="row-fluid form-authenticate">
        <div class="col-sm-12 col-md-8 col-md-offset-2">
            <h1 class="text-center login-title"><span class="logo">LabSpot</span></h1>
            <div class="splash-box">
                <tpl:block data="page.block.alerts" />
                <tpl:block data="page.block.body" />
                <hr />
                <tpl:import layout="console" />
            </div>
        </div>
    </div>
    </body>
    </html>
</tpl:layout>
