<!DOCTYPE html>
<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta name="description" content="${page.description}" />
        <meta name="author" content="${page.author}" />
        <meta name="keywords" content="${page.author}" />
        <link rel="shortcut icon" href="${config|design.theme}/assets/ico/favicon.png" />
        <title><tpl:element type="text" data="page.title">Budkit</tpl:element></title>
        <!-- Maverick core CSS -->
        <link href="${config|design.theme}/assets/css/bootstrap.css" rel="stylesheet" />
        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css' />
<!--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>-->
        <script data-main="${config|general.path}${config|design.theme}assets/js/main" src="${config|design.theme}/assets/js/require.js"></script>

        <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!--[if lt IE 9]>
        <script src="${config|design.theme}/assets/js/html5shiv.js"></script>
        <script src="${config|design.theme}/assets/js/respond.min.js"></script>
        <![endif]-->
        <tpl:utility type="head" />
    </head>

    <body>
        <!-- Fixed navbar -->

        <div class="column left minimize" id="column-left" data-remember-event="toggle">
            <tpl:import layout="navbar" />
            <tpl:menu id="dashboardmenu" type="nav-stacked menu" icons="true" />
            <ul class="nav nav-pills nav-stacked menu">
                <li class="nav-group"><a>Labels <i class="fa fa-plus pull-right"></i></a></li>
                <li><a href="#"><i class="menu-icon fa fa-circle color-alizarin"></i><span class="menu-text">Urgent</span></a></li>
                <li><a href="#"><i class="menu-icon fa fa-circle color-orange"></i><span class="menu-text">Result</span></a></li>
                <li><a href="#"><i class="menu-icon fa fa-circle color-sun-flower"></i><span class="menu-text">Requests</span></a></li>
                <li><a href="#"><i class="menu-icon fa fa-circle color-green-sea"></i><span class="menu-text">Follow Up</span></a></li>
            </ul>
        </div>

        <tpl:condition data="page.block.aside" test="isset" value="1">
            <div class="column aside"  id="column-aside">
                <tpl:block data="page.block.aside" />
            </div>
        </tpl:condition>

        <div class="column middle">
            <tpl:block data="page.block.alerts" />
            <tpl:condition data="page.block.tray" test="isset" value="1">
                <div class="scroll index tray">
                    <tpl:block data="page.block.tray" />
                </div>
            </tpl:condition>

            <tpl:condition data="page.block.side" test="isset" value="1">
            <div class="scroll index" id="block-side" data-remember-event="toggle">
                <tpl:block data="page.block.side" />
            </div>
            </tpl:condition>

            <div class="scroll content">
                <tpl:block data="page.block.body" />
                <tpl:import layout="footer" />
            </div>
        </div>



        <!-- Maverick core JavaScript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
<!--        <script src="${config|design.theme}/assets/js/mes/mediaelement-and-player.js"></script>-->
<!--        <script src="${config|design.theme}/assets/js/chosen/chosen.jquery.js"></script>-->
<!--        <script src="${config|design.theme}/assets/js/bootstrap.min.js"></script>-->
<!--        <script src="${config|design.theme}/assets/js/application.js"></script>-->
<!--        <script src="${config|design.theme}/assets/js/budkit.js"></script>-->
    </body>
</html>
</tpl:layout>

