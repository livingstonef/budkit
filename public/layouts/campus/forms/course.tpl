<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">

    <form action="/campus/course/create" class="form-vertical col-md-12 col-sm-12 col-lg-8" method="POST" enctype="multipart/form-data">
        <fieldset class="no-bottom-margin">
            <div class="control-group">
                <label class="control-label">Course Name <em class="mandatory">*</em></label>
                <div class="controls">
                    <input type="text" name="course_title" class="form-control margin-bottom-zero" />
                </div>
            </div>  
            <div class="control-group">
                <div class="controls">
                    <label class="control-label">Privacy</label>
                    <label class="radio">
                        <tpl:input type="radio" name="course_privacy" checked="checked"  value="public" />
                        <strong>Public</strong> - Anyone can view this course, any member can participate.
                    </label>
                    <label class="radio">
                        <tpl:input type="radio" name="course_privacy"  value="public,invite" />
                        <strong>Public, Invite Only</strong> - Anyone can view, but only invited members may participate 
                    </label>
                    <label class="radio">
                        <tpl:input type="radio" name="course_privacy"  value="private" />
                        <strong>Private</strong> - All your followers can view and join this course 
                    </label>
                    <label class="radio">
                        <tpl:input type="radio" name="course_privacy"  value="private,invite" />
                        <strong>Private, Invite Only</strong> - All your followers can view, but only invited members may participate
                    </label>
                    <label class="radio">
                        <tpl:input type="radio" name="course_privacy"  value="hidden" />
                        <strong>Hidden</strong> - Only you can use this course 
                    </label>
                </div>
            </div><!-- /control-group -->
            <div class="control-group">
                <label class="control-label"  for="middle-name">Cover photo</label>
                <div class="controls clearfix">
                    <input type="file" name="course_poster_image" data-label="Add Course Poster..." data-class="btn-info" data-target="budkit-uploader" />
                    <span class="help-block">.gif, .jpg, .jpeg or .png only</span>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" >Start Date</label>
                <div class="controls inline-inputs clearfix">
                    <div class="row">
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="day" value="" name="course_start_day"  class="form-control" /><!--disabled="1,2,3,4,5,6"-->
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="month" value="" name="course_start_month" class="form-control" />
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="year" value="" name="course_start_year" range="0" limit="+10" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label" >Stop Date</label>
                <div class="controls inline-inputs clearfix">
                    <div class="row">
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="day" value="" name="course_stop_day"  class="form-control" /><!--disabled="1,2,3,4,5,6"-->
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="month" value="" name="course_stop_month" class="form-control" />
                        </div>
                        <div class="col-md-2 col-sm-4">
                            <tpl:date type="year" value="" name="course_stop_year" range="0" limit="+10" class="form-control" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="control-group">
                <label class="control-label">Description</label>
                <div class="controls">
                    <textarea class="input-100pct focused" data-target="budkit-editor"  rows="10" name="course_description"></textarea>
                </div>
            </div>
            <div class="btn-toolbar margin-bottom-zero"> 
                <div class="btn-group">
                    <button type="submit" class="btn btn-primary" href="#">Submit</button>
                </div>
            </div>              
        </fieldset>
        <input type="hidden" name="${uploadprogress}" value="courseupload" />
    </form>
</tpl:layout>