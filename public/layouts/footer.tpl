<tpl:layout name="footer" xmlns:tpl="http://budkit.org/tpl">
    <tpl:block data="page.block.footer">Footer</tpl:block>
    <!-- Modal -->
    <div class="modal modal-large  fade"  id="ConsoleModal" tabindex="-1" role="dialog" aria-labelledby="ConsoleModal" aria-hidden="true">
        <div class="modal-dialog" data-spy="affix">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title"><tpl:element type="text" data="page.modal.title">Title</tpl:element></h4>
                </div>
                <div class="modal-body">
                    <tpl:import layout="console" />
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
    <tpl:import layout="modal" />
</tpl:layout>