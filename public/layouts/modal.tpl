<tpl:layout name="upload" xmlns:tpl="http://budkit.org/tpl">
    <!-- Modal -->
    <div class="modal fade" data-target="budkit-modal" tabindex="-1" role="dialog" aria-labelledby="SystemModal" aria-hidden="true">
        <div class="modal-dialog" data-spy="affix">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="SystemModal"><tpl:element type="text" data="page.modal.title">Title</tpl:element></h4>
                </div>
                <div class="modal-body">
                    <tpl:block data="page.modal.body" />
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary modal-submit">Finish</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
</tpl:layout>