<tpl:layout name="navbar" xmlns:tpl="http://budkit.org/tpl">
    <div class="navbar navbar-maverick">
        <div class="navbar-header minimize" id="off-canvas-toggle" data-remember-event="toggle">
            <a class="navbar-brand navbar-left" data-toggle="minimize"  data-target=".column.left,.navbar-header">BK</a>
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse hide-on-minimize">
            <ul class="nav navbar-nav navbar-right">
                <li class="hide-on-minimize minimize"><a href="/settings/member/account"><i class="fa fa-gear"></i></a></li>
            </ul>
        </div>
    </div>
    <!--{*<ul id="usermenu" icons="true" class="nav nav-stacked menu usermenu">*}
        {*<li><a href="/system/notifications/list" ><i class="menu-icon fa fa-warning"></i></a></li>*}
        {*<li><a href="/settings/member/account"><i class="menu-icon fa fa-gear"></i></a></li>*}
        {*<li><a data-toggle="#ConsoleModal"><i class="menu-icon fa fa-terminal"></i></a></li>*}
        {*<li><a href="/member/profile/logout"><i class="menu-icon fa fa-power-off"></i></a></li>*}
    {*</ul>*}-->
    <ul id="actionmenu" icons="true" label="Favourites" class="nav nav-stacked menu actionmenu">
        <tpl:condition  data="action.button" test="boolean" value="1" >
            <li>
                <a class="${action.button.class}" href="${action.button.link}">
                    <i class="menu-icon ${action.button.icon}"></i>
                    <span class="menu-text"><tpl:element type="text" data="action.button.text" /></span>
                </a>
            </li>
        </tpl:condition>
        <tpl:condition  data="action.button" test="boolean" value="0" >
            <li>
                <a class="btn-important" href="/system/media/create/editor">
                    <i class="menu-icon icon-share"></i>
                    <span class="menu-text">Create</span>
                </a>
            </li>
        </tpl:condition>
    </ul>
</tpl:layout>