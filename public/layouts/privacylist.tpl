<tpl:layout name="privacylist" xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <span class="help-inline">Privacy: </span>
    <a data-toggle="dropdown">Public</a>
    <ul class="dropdown-menu">
        <li><a href="#">Public</a></li>
        <li><a href="#">Private (Followers Only)</a></li>
        <li><a href="#">Hidden (Only Me)</a></li>
        <li class="divider"></li>
        <li><a href="/settings/member/privacy">Edit privacy</a></li>
    </ul>
</tpl:layout>