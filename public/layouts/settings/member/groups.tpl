<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="widget">
        <div class="widget-body">
            <div class="clearfix">
                <form class="form-horizontal" action="/settings/member/privacy/groups/edit" method="POST">
                    <div class="row">
                        <div class="col-md-4"><input type="text" name="group-title" class="form-control" placeholder="Group Name" /></div>

                        <tpl:condition data="groups" test="isset" value="1">
                            <div class="col-md-6">
                                <select name="group-parent" id="group-parent" class="form-control">
                                    <option value="">No Parent (Root)</option>
                                    <tpl:loop data="groups" id="authorities">
                                        <option value="${group_id}">
                                            <tpl:loop limit="indent"><span class="indenter">|--</span></tpl:loop>
                                            <span><tpl:element type="text" data="group_title" /></span>
                                        </option>
                                    </tpl:loop>
                                </select>
                            </div>
                        </tpl:condition>
                        <div class="col-md-2">
                            <input type="hidden" name="group-description" />
                            <button type="submit" class="btn btn-primary">Add Privacy Group</button>
                        </div>
                    </div>
                </form>
            </div>
            <hr />
            <tpl:condition data="groups" test="isset" value="0">
                <p class="placeholder-text">You have not created any privacy groups. Categorizing your followers into groups helps control the people with whom you share stuff.</p>
            </tpl:condition>
            <tpl:condition data="groups" test="isset" value="1">
                <table class="table">
                    <thead>
                    <tr>
                        <th class="col-md-8">Name</th>
                        <th class="col-md-2">Members</th>
                        <th class="col-md-2">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tpl:loop data="groups" id="authority-groups">
                        <tr>
                            <td class="authority-name">
                                <tpl:loop limit="indent"><span class="indenter">|--</span></tpl:loop>
                                <a href="/settings/member/privacy/group/${group_id}"><tpl:element type="text" data="group_title" /></a>
                            </td>
                            <td>0 members</td>
                            <td><a href="/settings/member/privacy/edit/${group_id}">Edit</a></td>
                        </tr>
                    </tpl:loop>
                    </tbody>
                </table>
            </tpl:condition>
        </div>
    </div>
</tpl:layout>
