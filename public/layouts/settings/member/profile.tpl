<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="widget">
        <div class="widget-body clearfix row">
            <form class="form-vertical col-md-12 col-sm-12 col-lg-8" method="post" enctype="multipart/form-data" action="/settings/member/profile/update">
                <div class="control-group">
                    <label class="control-label"  for="middle-name">Profile photo</label>
                    <div class="controls clearfix">
                        <tpl:condition data="profile.user_photo" test="isset" value="1">
                            <img class="thumbnail img-circle" src="/system/object/${profile.user_photo}/resize/200/200" />
                        </tpl:condition>
                        <input type="file" name="profilephoto" data-label="Change your profile picture..." data-class="btn-info" data-target="budkit-uploader" />
                        <span class="help-block">.gif, .jpg, .jpeg or .png only</span>
                    </div>
                </div>
                <hr />
                <fieldset>
                    <div class="control-group">
                        <label class="control-label"  for="profile[user_headline]">Profile Title</label>
                        <div class="controls">
                            <div class="input-append">
                                <input class="form-control" id="user_headline" name="profile[user_headline]" size="30" type="text" placeholder="E.g PhD Student, or Chief Executive Officer at Company X" value="${profile.user_headline}" />
                                <div class="btn-group">
                                    <tpl:import layout="privacylist" />
                                </div>
                            </div>
                        </div>
                    </div><!-- /control-group -->
                    <div class="control-group">
                        <label class="control-label"  for="profile[user_biography]">Biography</label>
                        <div class="controls">
                            <div class="input-append">
                                <textarea class="form-control pull-left" id="user_biography" name="profile[user_biography]" rows="7" ><tpl:element type="text" data="profile.user_biography" /></textarea>
                                <div class="btn-group">
                                    <tpl:import layout="privacylist" />
                                </div>
                            </div>
                        </div>
                    </div><!-- /control-group -->
                    <div class="control-group">
                        <label class="control-label"  for="profile[user_website]">Website</label>
                        <div class="controls">
                            <div class="input-append">
                                <input class="form-control" id="user_website" name="profile[user_website]" size="30" type="text" placeholder="http://yourwebsite.com" value="${profile.user_website}" />
                                <div class="btn-group">
                                    <tpl:import layout="privacylist" />
                                </div>
                            </div>
                        </div>
                    </div><!-- /control-group -->
                </fieldset>
                <div class="form-actions">
                    <button type="submit" class="btn btn-primary">Save changes</button>
                </div>
            </form>
        </div>
    </div>
</tpl:layout>