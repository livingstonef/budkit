<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
        <div class="widget">
            <div class="widget-body">
                <div class="clearfix">
                    <form class="form-horizontal" action="/settings/system/permissions/authorities/edit" method="POST">
                        <div class="row">
                            <div class="col-md-4"><input type="text" name="authority-title" class="form-control" placeholder="Group Name" /></div>
                            <div class="col-md-6">
                                <select name="authority-parent" id="authority-parent" class="form-control">
                                    <option value="">Select Parent</option>
                                    <tpl:loop data="authorities" id="authorities">
                                        <option value="${authority_id}">
                                            <tpl:loop limit="indent"><span class="indenter">|--</span></tpl:loop>
                                            <span><tpl:element type="text" data="authority_title" /></span>
                                        </option>
                                    </tpl:loop>
                                </select>
                            </div>
                            <div class="col-md-2">
                                <input type="hidden" name="authority-description" />
                                <button type="submit" class="btn btn-primary">Add New Group</button>
                            </div>
                        </div>
                    </form>
                </div>
                <hr />
                <div class="panel-group condensed" id="authority-group-collapse">
                    <tpl:loop data="authorities" id="authority-groups">
                        <div class="panel panel-default" id="authority-group-collapse">
                            <div class="panel-heading">
                                <div class="panel-title" data-toggle="collapse" data-parent="#authority-group-collapse" href="#group${authority_id}">
                                    <tpl:loop limit="indent"><span class="indenter">|--</span></tpl:loop>
                                    <a href="/settings/system/permissions/authority/${authority_id}"><span><tpl:element type="text" data="authority_title" /></span></a>
                                    <a href="/settings/system/permissions/authority/edit/${authority_id}"><span class="pull-right"><i class="fa fa-cog"></i></span></a>
                                </div>
                            </div>
                            <div id="group${authority_id}" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table table-striped">
                                        <thead>
                                            <th class="col-md-1">Type</th>
                                            <th class="col-md-2">Description</th>
                                            <th class="col-md-6">Area</th>
                                            <th class="col-md-2">Permission</th>
                                            <th>Action</th>
                                        </thead>
                                        <tbody>
                                            <tpl:loop data="permissions" id="authority-permissions">
                                                <tr>
                                                    <td><tpl:element type="text" data="permission_type" /></td>
                                                    <td><tpl:element type="text" data="permission_title" /></td>
                                                    <td><tpl:element type="text" data="permission_area_uri" /></td>
                                                    <td class="col-md-2"><tpl:element type="text" data="permission" /></td>
                                                    <td class="col-md-1"><a href="#">Revoke</a></td>
                                                </tr> 
                                            </tpl:loop>
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <form method="POST" action="/settings/system/permissions/addrule" class="no-margin">
                                                    <input type="hidden" name="area-authority" value="${authority_id}" />
                                                    <input type="hidden" name="authority-id"  value="${authority_id}" />
                                                    <td>
                                                        <select name="area-action" class="form-control native">
                                                            <option value="view">View</option>
                                                            <option value="execute">Execute</option>
                                                            <option value="modify">Modify</option>
                                                            <option value="special">Special</option>
                                                        </select>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="area-title"  placeholder="e.g Marketplace" class="form-control" />
                                                    </td>                                        
                                                    <td class="control-group">
                                                        <input type="text" name="area-uri" placeholder="e.g /marketplace/*" class="form-control" />
                                                    </td>                         
                                                    <td>
                                                        <select name="area-permission" class="form-control native">
                                                            <option value="inherit">Inherited</option>
                                                            <option value="allow">Allowed</option>
                                                            <option value="deny" selected="selected">Denied</option>
                                                        </select> 
                                                    </td>
                                                    <td>
                                                        <button type="submit" class="btn btn-default">Add</button>
                                                    </td>
                                                </form>
                                            </tr>
                                        </tfoot>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </tpl:loop>
                </div>
            </div>            
        </div>

</tpl:layout>
