<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <tpl:loop data="alerts">
        <div class="alert alert-banner alert-dismissable alert-${alertType}">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <strong><tpl:element type="text" data="alertTitle" /></strong> <tpl:element type="html" data="alertBody" />
        </div>
    </tpl:loop>
</tpl:layout>
