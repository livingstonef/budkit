<tpl:layout  name="timeline" xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="widget">
        <div class="widget-body">
            <div id="month-calendar" data-upload-action="/system/office/event/create.raw"></div>
        </div>
    </div>
    <script type="text/javascript">
        require(['main'], function(){
            require(["jquery","jquery.fullcalendar", "jquery.budkit"], function($, fullcalendar, budkit) {
                // initialize
                var date = new Date();
                var d = date.getDate();
                var m = date.getMonth();
                var y = date.getFullYear();

                var calendar = $('#month-calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay'
                    },
                    selectable: true,
                    selectHelper: true,
                    select: function(start, end, allDay) {
                        var eventform =  $(calendar).data("upload-action");
                        $(calendar).bkmodal({
                            title: 'Add Event',
                            action: eventform,
                            submit: function(modal, event){
                                var $form = modal.dialogbody.find('form:first'),
                                    $data = $form.submit();
                                    //1.json submit and wait for reply
                                    //2.get the json data for the vent and add to the table;
    //                             calendar.fullCalendar('renderEvent',
    //                                {
    //                                    title: title,
    //                                    start: start,
    //                                    end: end,
    //                                    allDay: allDay
    //                                },
    //                                true // make the event "stick"
    //                            );
                            }
                        });

                        calendar.fullCalendar('unselect');
                    },
                    editable: true,
                    events: [
                        {
                            title: 'All Day Event',
                            start: new Date(y, m, 1)
                        },
                        {
                            title: 'Long Event',
                            start: new Date(y, m, d-5),
                            end: new Date(y, m, d-2)
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: new Date(y, m, d-3, 16, 0),
                            allDay: false
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: new Date(y, m, d+4, 16, 0),
                            allDay: false
                        },
                        {
                            title: 'Meeting',
                            start: new Date(y, m, d, 10, 30),
                            allDay: false
                        },
                        {
                            title: 'Lunch',
                            start: new Date(y, m, d, 12, 0),
                            end: new Date(y, m, d, 14, 0),
                            allDay: false
                        },
                        {
                            title: 'Birthday Party',
                            start: new Date(y, m, d+1, 19, 0),
                            end: new Date(y, m, d+1, 22, 30),
                            allDay: false
                        },
                        {
                            title: 'Click for Google',
                            start: new Date(y, m, 28),
                            end: new Date(y, m, 29),
                            url: 'http://google.com/'
                        }
                    ]
                });
            });
        })
    </script>
</tpl:layout>