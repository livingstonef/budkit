<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <tpl:condition test="isnot" data="page.format" value="raw">
        <tpl:condition test="isset" data="page.block.dashboardtoolbar" value="1">
            <div class="toolbar toolbar-maverick">
                <div class="toolbar-collapse collapse">
                    <div class="toolbar-text"><tpl:element type="text" data="page.title" /></div>
                    <tpl:block data="page.block.dashboardtoolbar" />
                </div>
            </div>
        </tpl:condition>
        <div class="widget">
            <tpl:condition test="isset" data="page.subtitle" value="1">
                <div class="widget-head">
                    <span class="widget-title"><tpl:element type="text" data="page.subtitle">Dashboard</tpl:element></span>
                </div>
            </tpl:condition>
            <div class="widget-body">
                <tpl:block data="page.block.dashboard" />
            </div>
        </div>
    </tpl:condition>
    <tpl:condition test="equals" data="page.format" value="raw">
         <tpl:block data="page.block.dashboard" />
    </tpl:condition>
</tpl:layout>
