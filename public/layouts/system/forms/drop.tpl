<tpl:layout  xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <form action="/system/content/create" method="POST" class="form-vertical margin-bottom-zero"  enctype="multipart/form-data">
        <div id="upload-dropbox" class="well text-center">
            <div class="upload-bucket clearfix" data-src="${config|general.path}system/media/attachments/"></div>
            <div class="upload-drop clearfix">
                <div class="upload-prompt" align="center">Drop files here to upload</div>
                <small style="font-size: 10px; margin: 20px 0; display: block">- OR -</small>
                <input type="file" name="mediaobjects[]" multiple="" data-target="budkit-uploader" data-class="btn-info" data-display=".upload-bucket" data-label="Upload Files" autoload="" />
            </div>
            <tpl:condition test="equals" data="page.format" value="raw">
                <hr />
                <div class="control-group">
                    <div class="controls row-fluid">
                        <input type="text" name="drop-url" class="form-control" placeholder="Enter external URL" />
                    </div>
                </div>
                <div class="control-group">
                    <div class="controls row-fluid">
                        <input type="text" name="drop-search" class="form-control" placeholder="Search Existing" />
                    </div>
                </div>
            </tpl:condition>
        </div>
    </form>
    <script type="text/javascript">
        require(['main'], function(){
            require(["jquery","budkit.uploader"], function($) {
                $('[data-target="budkit-uploader"]').bkuploader();
            });
        })
    </script>
</tpl:layout>