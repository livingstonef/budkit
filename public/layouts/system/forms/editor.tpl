<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <form action="/system/media/timeline/create" class="no-margin" method="POST" enctype="multipart/form-data">
        <div class="editor fill-container">
            <div class="editor-tray hide">
                <div class="toolbar toolbar-maverick">
                    <div class="toolbar-collapse collapse">
                        <div class="toolbar-text">Some Items</div>
                    </div>
                </div>
            </div>
            <div class="editor-aside hide">
                <div class="toolbar toolbar-maverick">
                    <div class="toolbar-collapse collapse">
                        <div class="toolbar-text">Objects</div>
                    </div>
                </div>
            </div>
            <div class="editor-document">
                <div class="toolbar toolbar-maverick">
                    <div class="toolbar-container-fluid">
                        <div class="toolbar-header">
                            <button type="button" class="toolbar-toggle" data-toggle="collapse" data-target=".toolbar-collapse">
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                        </div>
                        <div class="toolbar-collapse">
                            <ul class="nav toolbar-nav">
                                <li><a href="#" data-toggle="dropdown" class="dropdown-toggle">File</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">New Report</a></li>
                                        <li><a href="#">New Presentation</a></li>
                                        <li><a href="#">New Spreadsheet</a></li>
                                        <li><a href="#">New To-do list</a></li>
                                        <li><a href="#">Import File</a></li>
                                        <li class="divider"></li>
                                        <li class="dropdown-header">Templates</li>
                                        <li><a href="#">Invoice Template</a></li>
                                        <li><a href="#">Web Page Template</a></li>
                                    </ul>
                                </li>

                                <li>
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Edit</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Undo</a></li>
                                        <li><a href="#">Redo</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Cut</a></li>
                                        <li><a href="#">Copy</a></li>
                                        <li><a href="#">Paste</a></li>
                                        <li><a href="#">Delete</a></li>
                                        <li><a href="#">Select All</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Spell Check</a></li>
                                        <li><a href="#">Find</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">View</a>
                                    <ul class="dropdown-menu">

                                        <li><a href="#">Full Screen</a></li>
                                        <li><a href="#">Presentation Mode</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Collaborators</a></li>
                                        <li><a href="#">Blame</a></li>
                                        <li><a href="#">Revisions</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">View Source</a></li>
                                        <li><a href="#" data-toggle="hide" data-target=".editor-tray">View Thumbnails</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Format</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Font...</a></li>
                                        <li><a href="#">Paragraph...</a></li>
                                        <li><a href="#">Headings...</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Style</a></li>
                                    </ul>
                                </li>
                                <li>
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Insert</a>
                                    <ul class="dropdown-menu">
                                        <li><a href="#">Photo...</a></li>
                                        <li><a href="#">Audio...</a></li>
                                        <li><a href="#">Video...</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Shape...</a></li>
                                        <li><a href="#">Random Text...</a></li>
                                        <li><a href="#">Page Break</a></li>
                                        <li class="divider"></li>
                                        <li><a href="#">Hyperlink...</a></li>
                                        <li><a href="#" data-toggle="hide" data-target=".editor-aside">Drag and Drop</a></li>
                                    </ul>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="editor-panel">
                    <div class="editor-page" contenteditable="true" data-api="budkit"></div>
                </div>
            </div>
        </div>
        <input type="hidden" name="${uploadprogress}" value="timelineupload"/>
        <input type="hidden" name="media_target" value="${media.target}"/>
        <input type="hidden" name="media_author_id" value="${media.authorid}"/>
        <input type="hidden" name="media_verb" value="${media.verb}"/>
        <input type="hidden" name="media_provider" value="${media.provider}"/>
    </form>
</tpl:layout>