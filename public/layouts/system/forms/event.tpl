<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <form action="/system/office/event/create" id="createevent" method="POST" enctype="multipart/form-data">
        <div class="control-group">
            <label class="control-label">Event title <em class="mandatory">*</em></label>
            <div class="controls row-fluid">
                <input type="text" name="media_title" class="form-control" required=""  />
            </div>
        </div>
        <div class="control-group">
            <label class="control-label">Location</label>
            <div class="controls row-fluid">
                <input type="text" name="event_location" class="form-control" />
            </div>
        </div>
        <div class="row">
            <div class="control-group col-md-6">
                <label class="control-label">Start Date</label>
                <div class="controls row-fluid">
                    <input type="date" name="event_start_date" required="" class="form-control" />
                </div>
            </div>
            <div class="control-group col-md-6">
                <label class="control-label">Stop Date</label>
                <div class="controls row-fluid">
                    <input type="date" name="event_stop_date" class="form-control" />
                </div>
            </div>
        </div>
        <div class="row">
            <div class="control-group col-md-6">
                <label class="control-label">Start Time</label>
                <div class="controls row-fluid">
                    <input type="time" name="event_start_time" class="form-control" />
                </div>
            </div>
            <div class="control-group col-md-6">
                <label class="control-label">Stop Time</label>
                <div class="controls row-fluid">
                    <input type="time" name="event_stop_time" class="form-control" />
                </div>
            </div>
        </div>
        <tpl:condition test="equals" data="page.format" value="xhtml">
            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Submit" />
            </div>
        </tpl:condition>
    </form>
</tpl:layout>