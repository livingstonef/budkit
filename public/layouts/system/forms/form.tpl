
<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <tpl:condition test="compare" count="1" operator="equals" value="1" data="editor_forms">
        <tpl:import layout="input" />
    </tpl:condition>
    <tpl:condition test="compare" count="1" operator="greaterthan" value="1" data="editor_forms">
        <div class="toolbar toolbar-maverick">
            <div class="toolbar-collapse collapse">
                <div class="toolbar-text">Participate</div>
            </div>
        </div>
        <div class="form-search">
            <form role="search"  action="/system/search/graph" method="post">
                <input type="text" class="form-control" name="query" placeholder="Search for anything" />
            </form>
        </div>
        <div class="stream-group">

            <div class="stream-list form-list">
                <ul class="stream">
                    <tpl:loop id="form-launchers" data="editor_forms">
                        <li class="stream-item bottom-border roll">
                            <a href="/system/media/create/${id}" class="has-featured-img link">
                                <button type="button" class="btn btn-default background-clouds btn-circle featured-media btn-lg"><i class="icon ${icon-class}"></i></button>
                                <span class="subject"><tpl:element type="text" data="title" /></span>
                                <div class="content clearfix">
                                    <span class="help-block margin-zero"><tpl:element type="text" data="hint" /></span>
                                    <span class="metric"><tpl:element type="text" data="metric" /></span>
                                </div>
                            </a>
                        </li>
                    </tpl:loop>
                </ul>
            </div>
        </div>
    </tpl:condition>
</tpl:layout>