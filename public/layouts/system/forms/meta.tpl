<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">

    <div class="panel panel-input">
        <div class="panel-heading">
            <span class="panel-title">Featured Media</span>
        </div>
        <div class="panel-body">
            <tpl:condition data="media.object" test="isset" value="1">
                <tpl:media uri="media.object" class="media clearfix img-responsive" link="true" data-target="budkit-slider" />
                <!--<span class="help-block">e.g. a Video, Audio, Image, Playlist or Collection</span>-->
                <input type="file" name="mediaobjects[]" data-label="Modify or Change featured media..." data-class="btn-info form-control" data-target="budkit-uploader" />
            </tpl:condition>
            <!--<span class="help-block">e.g. a Video, Audio, Image, Playlist or Collection</span>-->
            <tpl:condition data="media.object" test="isset" value="0">
                <input type="file" name="mediaobjects[]" data-label="Choose featured media..." data-class="btn-default form-control" data-target="budkit-uploader" />
            </tpl:condition>
        </div>
    </div>

    <div class="control-group">
        <div class="panel panel-input">
            <div class="panel-heading">
                <span class="panel-title">Categories</span>
            </div>
            <div class="panel-body">
                <div class="multiple-select">
                    <tpl:select class="form-control" multiple="multiple" data-placeholder="Choose a category...">
                        <option value="1">Biotechnology</option>
                        <option value="1">Chemistry</option>
                        <option value="1">Zoology</option>
                        <option value="1">Physics</option>
                        <option value="1">Geography</option>
                        <option value="1">Radiology</option>
                    </tpl:select>
                </div>
            </div>
        </div>
    </div>
    <div class="control-group">
        <div class="panel panel-input">
            <div class="panel-heading">
                <span class="panel-title">Tags</span>
            </div>
            <div class="panel-body">
                <input type="text" placeholder="Title (Optional) " class="form-control margin-bottom-zero" value="${media_summary}" />
            </div>
        </div>
    </div>

</tpl:layout>