<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">

    <form class="form-signin" name="login_form" action="/system/authenticate/login" method="post">

        <tpl:condition data="tempauth" test="isset" value="1">
            <img class="profile-img dp img-circle" src="${tempauth.image}" alt="" />
            <span class="help-block"><tpl:element type="text" formatting="sprintf" cdata="Pair an existing account with this %s account" data="tempauth.provider" /></span>
            <input name="user_${tempauth.provider}" value="${tempauth.token}" type="hidden" />
        </tpl:condition>

        <tpl:condition data="tempauth" test="isset" value="0">
            <tpl:condition data="alternatives" test="isset" value="1">

                <span class="help-block">Sign In with..</span>
                <ul class="list-unstyled">
                    <tpl:loop data="alternatives" id="login-alt">
                        <tpl:condition data="link" test="isset" value="1">
                            <li class="inline-block"><a href="${link}" class="btn btn-lg btn-primary btn-block btn-${uid}"><tpl:element type="text" data="title" /></a></li>
                        </tpl:condition>
                    </tpl:loop>
                </ul>

                <hr />
            </tpl:condition>
        </tpl:condition>

        <input type="text" id="user_name_id" name="user_name_id" class="form-control" placeholder="Email" value="${tempauth.email}" required="true" autofocus="true" />
        <input type="password" id="user_password" name="user_password" class="form-control" placeholder="Password" required="true" />
        <input type="hidden" name="handler" value="dbauth" />
        <input type="hidden" name="redirect" value="${lasturl}" />

        <button class="btn btn-lg btn-primary btn-block action-btn" type="submit">
            Sign in</button>
        <label class="checkbox pull-left">
            <input type="checkbox" value="remember-me" />
            Remember me
        </label>
        <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
    </form>


    <tpl:condition test="boolean" data="config|general.site-allow-registration" value="1">
        <hr />
        <a href="/system/authenticate/create" class="text-center new-account">Don't Have an Account? Sign-Up now for FREE </a>
    </tpl:condition>

</tpl:layout>