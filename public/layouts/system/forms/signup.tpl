<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">

    <form class="form-signin" name="signup_form" action="/sign-up" method="post">

        <tpl:condition data="tempauth" test="isset" value="1">
            <img class="profile-img dp img-circle" src="${tempauth.image}" alt="" />
            <span class="help-block"><tpl:element type="text" formatting="sprintf" cdata="Pair an existing account with this %s account" data="tempauth.provider" /></span>
            <input name="user_${tempauth.provider}" value="${tempauth.token}" type="hidden" />
        </tpl:condition>

        <tpl:condition data="tempauth" test="isset" value="0">
            <h1 class="text-center login-title">Sign Up for new account</h1>
            <tpl:condition data="alternatives" test="isset" value="1">
                <ul class="list-unstyled">
                    <tpl:loop data="alternatives" id="login-alt">
                        <tpl:condition data="link" test="isset" value="1">
                            <li class="inline-block"><a href="${link}" class="btn btn-lg btn-primary btn-block btn-${uid}"><tpl:element type="text" data="title" /></a></li>
                        </tpl:condition>
                    </tpl:loop>
                </ul>
                <hr />
            </tpl:condition>
        </tpl:condition>

        <tpl:condition test="boolean" data="config|general.site-allow-registration" value="0">
            <div class="alert alert-info">We are currently not accepting any new user registration at this time. Please try again later</div>
        </tpl:condition>
        <tpl:condition test="boolean" data="config|general.site-allow-registration" value="1">
            <tpl:condition test="boolean" data="config|general.site-inviteonly" value="0">

                <input type="text" id="user_first_name" name="user_first_name" class="form-control" placeholder="First Name" value="${tempauth.first_name}" required="true"  />
                <input type="text" id="user_last_name" name="user_last_name" class="form-control" placeholder="Last Name" value="${tempauth.last_name}" required="true"  />

                <input type="text" id="user_email" name="user_email" class="form-control" placeholder="E-mail address" value="${tempauth.email}" required="true"  />
                <input type="text" id="user_name_id" name="user_name_id" class="form-control" placeholder="Unique username" value="${tempauth.nickname}" required="true" />

                <input type="password" id="user_password" name="user_password" class="form-control" placeholder="Password" required="true" />
                <input type="password" id="user_password" name="user_password_2" class="form-control" placeholder="Verify Password" required="true" />

            </tpl:condition>
            <tpl:condition test="boolean" data="config|general.site-inviteonly" value="1">
                <input  type="text" id="user_invite_code" name="user_invite_code" class="form-control" placeholder="Invite Code"/>
            </tpl:condition>

        </tpl:condition>

        <button class="btn btn-lg btn-primary btn-block action-btn" type="submit">
            Create an Account</button>
        <label class="checkbox pull-left">
            <input type="checkbox" name="user_accepted_terms" value="1" />
            <input type="hidden" name="user_accepted_terms_2" value="2" />
            You accept our Terms
        </label>
        <a href="#" class="pull-right need-help">Need help? </a><span class="clearfix"></span>
    </form>


    <tpl:condition test="boolean" data="config|general.site-allow-registration" value="1">
        <hr />
        <a href="/system/authenticate/login" class="text-center new-account">Already have an account? </a>
    </tpl:condition>

</tpl:layout>