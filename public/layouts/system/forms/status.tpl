<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <form action="/system/media/timeline/create" class="no-margin" method="POST" enctype="multipart/form-data">
        <div class="panel panel-input">
            <div class="panel-heading panel-heading-toolbar"></div>
            <div class="panel-body">
                <div class="control-group">
                    <div class="controls panel-editor-container"> <!--{*data-toolbar-tools='[["bold", "italic", "underline", "strikethrough"],["link", "unlink", "image"]]'*}-->
                        <textarea class="form-control" data-target="budkit-editor" toolbar="1"
                                  data-toolbar-tools="${config|content.status-tools}"
                                  data-upload-action="/system/media/create/drop.raw"
                                  id="editor"  rows="3"
                                  name="media_content"
                                  placeholder="Say something">
                            <tpl:element type="html" data="media.content" medialinks="true" />
                        </textarea>
                    </div>
                </div>
            </div>
            <div class="status-bucket margin-top-half" data-src="${config|general.path}system/media/attachments/"></div>
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary btn-sm" href="#">Publish</button>
            </div>
        </div>
        <input type="hidden" name="${uploadprogress}" value="timelineupload" />
        <input type="hidden" name="media_target" value="${mediatarget}" />
        <input type="hidden" name="media_author_id" value="${mediaauthorid}" />
        <input type="hidden" name="media_verb" value="${mediaverb}" />
        <input type="hidden" name="media_provider" value="${mediaprovider}" />
    </form>
</tpl:layout>