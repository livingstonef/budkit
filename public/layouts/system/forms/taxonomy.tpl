<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="row">
        <form class="form-vertical col-md-12 col-sm-12 col-lg-8" action="/settings/member/profile/update"  method="post" enctype="multipart/form-data" >
            <div class="control-group">
                <label class="control-label"  for="taxonomy_title">Title</label>
                <div class="controls">
                    <input class="form-control" id="first-name" name="taxonomy_title" size="30" type="text" value="${taxonomy_title}" />
                </div>
            </div><!-- /control-group -->
            <tpl:condition data="taxonomies" test="isset" value="1">
                <div class="control-group">
                    <label class="control-label"  for="taxonomy_title">Parent</label>
                    <div class="controls">
                        <select name="group-parent" id="group-parent" class="form-control">
                            <option value="">No Parent (Root)</option>
                            <tpl:loop data="groups" id="authorities">
                                <option value="${group_id}">
                                    <tpl:loop limit="indent"><span class="indenter">|--</span></tpl:loop>
                                    <span><tpl:element type="text" data="group_title" /></span>
                                </option>
                            </tpl:loop>
                        </select>
                    </div>
                </div><!-- /control-group -->
            </tpl:condition>
            <div class="control-group">
                <div class="controls">
                    <div class="panel panel-input">
                        <div class="panel-heading panel-heading-toolbar" id="toolbar-holder"></div>
                        <div class="panel-body">
                            <div class="control-group">
                                <div class="controls">
                                    <textarea class="form-control" data-target="budkit-editor" toolbar="1" data-toolbar-target="#toolbar-holder"  id="editor"  rows="10" name="media_content" placeholder="Category Description">
                                        <tpl:element type="html" data="media.content" medialinks="true" />
                                    </textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- /control-group -->

            <div class="panel panel-input">
                <div class="panel-heading">
                    <span class="panel-title">Cover Image</span>
                </div>
                <div class="panel-body">
                    <tpl:condition data="media.object" test="isset" value="1">
                        <tpl:media uri="media.object" class="media clearfix img-responsive" link="true" data-target="budkit-slider" />
                        <!--<span class="help-block">e.g. a Video, Audio, Image, Playlist or Collection</span>-->
                        <input type="file" name="mediaobjects[]" data-label="Modify or Cover Image" data-class="btn-info form-control" data-target="budkit-uploader" />
                    </tpl:condition>
                    <!--<span class="help-block">e.g. a Video, Audio, Image, Playlist or Collection</span>-->
                    <tpl:condition data="media.object" test="isset" value="0">
                        <input type="file" name="mediaobjects[]" data-label="Choose Cover Image" data-class="btn-info form-control" data-target="budkit-uploader" />
                    </tpl:condition>
                </div>
            </div>

            <div class="form-actions">
                <input type="submit" class="btn btn-primary" value="Add Category" />
            </div>
        </form>
    </div>
</tpl:layout>