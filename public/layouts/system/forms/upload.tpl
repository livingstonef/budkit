<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <form action="/system/media/timeline/create" class="no-margin" method="POST" enctype="multipart/form-data">
        <div class="row">
            <div class="col-md-12">
                <tpl:import layout="media/gallery" app="system" />
            </div>
        </div>
        <div class="bucket margin-top-half" data-src="${config|general.path}system/media/attachments/"></div>

    </form>
</tpl:layout>