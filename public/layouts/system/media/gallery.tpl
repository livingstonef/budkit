<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <tpl:condition data="gallery" test="isset" value="1">
        <tpl:import layout="media/gallery/widget" />
    </tpl:condition>
    <tpl:condition data="gallery" test="isset" value="0">
        <p class="placeholder-text padding">There are no items to display. :(</p>
    </tpl:condition>
    <tpl:import layout="pagination" />
</tpl:layout>
