<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="navbar navbar-default">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a data-target="#media-gallery"  data-toggle="media-list media-grid" title="Grid"><i class="fa fa-th icon-16"></i></a></li>
                <li><a data-target="#media-gallery"  data-toggle="media-grid media-list" title="List"><i class="fa fa-th-list icon-16"></i></a></li>
            </ul>
            <tpl:menu id="mediamenu" type="nav navbar-nav navbar-left" />
        </div><!--/.nav-collapse -->
    </div>
</tpl:layout>