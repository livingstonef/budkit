<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">            
    <ul class="stream media-grid compensate-margins" id="media-gallery">
        <tpl:loop data="gallery.items" id="gallery-items">
            <li class="mgrow-fluid padding-zero">
                <div class="feature mgcol1 list-hide">
                    <tpl:media uri="object_uri" link="true" mode="detailed"  class="media-item" width="${config|content.gallery-thumbnail-width}" height="${config|content.gallery-thumbnail-height}" />
                    <ul class="nav nav-pills feature-tools">
                        <li class="action-fans pull-left">
                            <a href="/system/media/attachments/view/${object_uri}"><i class="fa fa-eye"></i></a>
                        </li>

                        <li class="action-edit pull-left">
                            <a href="/system/object/${object_uri}" target="_blank"><i class="fa fa-cloud-download"></i></a>
                        </li>
                        <li class="action-fans pull-right">
                            <a href="/system/media/attachments/delete/${object_uri}"><i class="fa fa-trash-o"></i></a>
                        </li>
                    </ul>
                </div>   
                <!--<input type="checkbox" class="select" />-->
                <a href="/system/media/attachments/view/${object_uri}" class="grid-hide has-featured-img link ${message_status}">
                    <tpl:media uri="object_uri" mode="icon"  class="featured-img" width="65" height="65" />
                    <div class="subject">
                        <tpl:element type="text" data="attachment_title"/>
                    </div>
                    <div class="title">
                        <span class="auhor"><tpl:element type="text" data="message_author.user_full_name" /></span>
                        <span class="time"><tpl:element type="time" data="object_created_on"/></span>
                    </div>
                    <div class="content clearfix">
                        <tpl:element type="text" data="attachment_type"/>
                        <span class="help-inline"><tpl:element type="text" data="attachment_size"/></span>
                    </div>
                </a>
            </li>
        </tpl:loop>
    </ul>  
</tpl:layout>