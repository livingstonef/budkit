<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <ul class="media-grid media-gallery compensate-margins media-grid-2">
        <tpl:loop data="repository.items" id="repository-items">
            <li class="mgrow-fluid padding-zero">
                <div class="repository-item clearfix">
                    <div class="row">
                        <a class="col-lg-4 col-md-12 col-xs-12 featured-img text-center">
                            <img class="img-responsive" src="https://wac.A8B5.edgecastcdn.net/80A8B5/achievement-images/flags/ipad_default_2x_cover-html-accessibility.png" />
                        </a>
                        <div class="col-lg-8 description visible-lg">
                            <div class="caption">
                                <h2>Biotechnology</h2>
                                <p>My project description</p>
                                <hr />
                                <div class="row counts">
                                    <div class="col-md-4"><b>5765</b><br/><small>Modules</small></div>
                                    <div class="col-md-4"><b>87</b><br/><small>Activities</small></div>
                                    <div class="col-md-4"><b>789</b><br/><small>Participants</small></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
        </tpl:loop>
    </ul>
</tpl:layout>