<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="stream-group">
        <div class="stream-title">Life Sciences</div>
        <div class="stream-list">
            <ul class="stream">
                <li class="stream-item bottom-border roll">
                    <!--<input type="checkbox" class="select" />-->
                    <a href="#" class="link has-featured-img">
                        <img src="//lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=60"  class="featured-img media-object dp img-circle" width="60" height="60" />
                        <div class="subject">
                            Biochemistry
                        </div>
                        <div class="title">
                            <span class="auhor">4768 Protocols</span>
                            <span class="time">242 disciplines</span>
                        </div>
                        <div class="content clearfix">
                            Lorem Ipsum description
                        </div>
                    </a>
                </li>
                <li class="stream-item bottom-border">
                    <!--<input type="checkbox" class="select" />-->
                    <a href="#" class="link has-featured-img">
                        <img src="//lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=60"  class="featured-img media-object dp img-circle" width="60" height="60" />
                        <div class="subject">
                            Bioinformatics
                        </div>
                        <div class="title">
                            <span class="auhor">4768 Protocols</span>
                            <span class="time">242 disciplines</span>
                        </div>
                        <div class="content clearfix">
                            Lorem Ipsum description
                        </div>
                    </a>
                </li>
                <li class="stream-item bottom-border">
                    <!--<input type="checkbox" class="select" />-->
                    <a href="#" class="link has-featured-img">
                        <img src="//lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=60"  class="featured-img media-object dp img-circle" width="60" height="60" />
                        <div class="subject">
                            Biotechnology
                        </div>
                        <div class="title">
                            <span class="auhor">4768 Protocols</span>
                            <span class="time">242 disciplines</span>
                        </div>
                        <div class="content clearfix">
                            Lorem Ipsum description
                        </div>
                    </a>
                </li>
                <li class="stream-item bottom-border">
                    <!--<input type="checkbox" class="select" />-->
                    <a href="#" class="link has-featured-img">
                        <img src="//lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=60"  class="featured-img media-object dp img-circle" width="60" height="60" />
                        <div class="subject">
                            Cell Biology
                        </div>
                        <div class="title">
                            <span class="auhor">4768 Protocols</span>
                            <span class="time">242 disciplines</span>
                        </div>
                        <div class="content clearfix">
                            Lorem Ipsum description
                        </div>
                    </a>
                </li>
                <li class="stream-item bottom-border">
                    <!--<input type="checkbox" class="select" />-->
                    <a href="#" class="link has-featured-img">
                        <img src="//lh5.googleusercontent.com/-b0-k99FZlyE/AAAAAAAAAAI/AAAAAAAAAAA/eu7opA4byxI/photo.jpg?sz=60"  class="featured-img media-object dp img-circle" width="60" height="60" />
                        <div class="subject">
                            Developmental Biology
                        </div>
                        <div class="title">
                            <span class="auhor">4768 Protocols</span>
                            <span class="time">242 disciplines</span>
                        </div>
                        <div class="content clearfix">
                            Lorem Ipsum description
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</tpl:layout>