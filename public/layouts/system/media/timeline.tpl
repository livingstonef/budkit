<tpl:layout  name="timeline" xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">

    <ol class="stream detailed" data-timeline="true">
        <tpl:loop data="activities.items" id="timeline-items">
            <li class="stream-item">
                <div class="item reading holder has-featured-img">
                    <tpl:condition data="object" test="isset" value="1">
                        <div class="stream-media">
                            <tpl:media uri="object" class="media clearfix img-responsive" link="true" data-target="budkit-slider" />
                        </div>
                    </tpl:condition>
                    <tpl:condition test="boolean" data="singular" value="1">
                        <tpl:condition data="title" test="isset" value="1">
                            <h2 class="stream-subject"><tpl:element type="text" data="title" medialinks="true" /></h2>
                        </tpl:condition>
                    </tpl:condition>
                    <div class="stream-content">
                        <a class="featured-img" href="/member:${actor.uri}/profile/timeline">
                            <img class="media-object dp" src="${actor.image.url}" alt="${actor.displayName}" width="${actor.image.width}" height="${actor.image.height}" />
                        </a>
                        <!--<div class="extra"><a class="btn btn-circle">100</a></div>-->
                        <div class="content clearfix">
                            <div class="title">
                                <span class="author"><tpl:element type="text" data="actor.displayName" /></span>
                                <a href="/system/media/timeline/view/${uri}/${type}" title="${published}" class="time pull-right"><tpl:element type="time" data="published" /></a>
                            </div>
                            <tpl:element type="html" data="content" medialinks="true" />

                            <!--<div class="meta">
                                <a  href="#" class="tag tag-default bioinformatics">Bio-Informatics</a>
                                <a  href="#" class="tag tag-default bioinformatics">Phyton</a>
                                <a  href="#" class="tag tag-default bioinformatics">Command Line</a>
                            </div>-->
                            <div class="actions">
                                <ul class="nav nav-pills pull-left">
                                    <li class="action-fans"><a href="/system/media/timeline/fans/${uri}">Like</a></li>
                                    <tpl:condition data="target_count" test="isset" value="1">
                                        <li class="action-comments"><a href="/system/media/timeline/view/${uri}#comments"><tpl:element type="text" formatting="sprintf" cdata="%d Replies" data="target_count"  /></a></li>
                                    </tpl:condition>
                                    <tpl:condition data="target_count" test="isset" value="0">
                                        <li class="action-comments"><a data-target=".comments-${uri}" data-toggle="hide">Comment</a></li>
                                    </tpl:condition>
                                    <li class="action-edit"><a href="/system/media/timeline/edit/${uri}/type:${type}">Edit</a></li>
                                </ul>
                                <ul class="nav nav-pills pull-right">
                                    <li class="action-delete"><a href="/system/media/timeline/delete/${uri}/type:${type}">Trash</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="stream-list comments-${uri} hide">
                    <tpl:import layout="forms/comment" />
                </div>
            </li>
        </tpl:loop>
        <!--<li class="stream-item">
            <a class="more">More ...</a>
        </li>-->
    </ol>
</tpl:layout>