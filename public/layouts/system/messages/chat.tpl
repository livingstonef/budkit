<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="toolbar toolbar-maverick">
        <ul class="nav toolbar-nav toolbar-right">
            <li><a href="" data-toggle="hide hover" data-target=".column.aside"><i class="fa fa-users"></i></a></li>
            <li><a><i class="fa fa-plus"></i></a></li>
            <li><a><i class="fa fa-gear"></i></a></li>
        </ul>
    </div>
    <div class="chat-input">
        <tpl:import layout="input" />
    </div>
    <div class="chat-thread stream-thread">
        <div class="stream-group">
            <div class="stream-list">
                <tpl:import layout="media/timeline" />
            </div>
        </div>
    </div>
</tpl:layout>