<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="toolbar toolbar-maverick">
        <div class="toolbar-text">Collaborators</div>
    </div>
    <div>
        <ul class="nav nav-list">
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Peter Chater</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Tanya Vyland</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Joshua Fultang</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Rudolf Sanchez</a></li>                  
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence away"></i>  Peter Rachmore</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i>  Esteban M.</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i>  Tatiana Menandez</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence busy"></i> Sophia Young</a></li>                      
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Rita Derry</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Ophelia Bains</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Stanislas Kopyov</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence offline"></i> Bronco Cho</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Feng Cheng</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence busy"></i> Barry Burst</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i>  Tatiana Menandez</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence busy"></i> Sophia Young</a></li>                      
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Rita Derry</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Ophelia Bains</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Stanislas Kopyov</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence offline"></i> Bronco Cho</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence online"></i> Feng Cheng</a></li>
            <li><a href="/system/messages/chat/live/livingstonef"><i class="presence busy"></i> Barry Burst</a></li>
        </ul>
    </div>
</tpl:layout>