<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <div class="toolbar toolbar-maverick">
        <div class="toolbar-collapse collapse">
            <div class="toolbar-text">Messages</div>
        </div>
    </div>
    <div class="stream-group">
        <div class="stream-list">
            <ul class="stream">
                <tpl:condition test="boolean" data="messages.totalItems" value="0">
                    <li class="stream-item padding">No Messages to display :)</li>
                </tpl:condition>
                <tpl:loop data="messages.items" id="message-list">
                    <li class="stream-item bottom-border roll">
                        <a href="/system/message/inbox/${object_uri}" class="link  ${message_status}">
                            <tpl:condition test="isset" data="message_subject" value="1">
                                <div class="subject">
                                    <tpl:element type="text" data="message_subject" />
                                </div>
                            </tpl:condition>
                            <div class="title">
                                <span class="auhor"><tpl:element type="text" data="message_author.user_full_name" /></span>
                                <span class="time"><tpl:element type="time" data="object_updated_on" /></span>
                            </div>
                            <div class="content clearfix">
                                <tpl:element type="text" data ="message_body" wordlimit="10" />
                            </div>
                        </a>
                    </li>
                </tpl:loop>
            </ul>
        </div>
    </div>
</tpl:layout>