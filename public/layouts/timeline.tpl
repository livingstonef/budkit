<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
    <tpl:condition test="isset" data="activities.items" value="1">
        <div class="stream-grid" id="timeline">
            <tpl:import layout="media/timeline" />
        </div>
    </tpl:condition>
    <tpl:condition test="boolean" data="activities.items" value="0">
        <p>There are no items to display. :(</p>
    </tpl:condition>
    <script type="text/javascript">
        require(['main'], function(){
            require(["jquery","jquery.bridget","masonry"], function($, bridget, Masonry) {
                bridget( 'masonry', Masonry );
                // initialize
                $('.stream-grid').masonry({"columnWidth":418,"itemSelector":".stream-item","gutter":15,"stamp": ".fixed-item" });
                //Make sure all media is loaded, and then build layout again!
                $(window).load( function() {
                    $('.stream-grid').masonry();
                }).on('resize', function(){
                    $('.stream-grid').masonry();
                });
            });
        })
    </script>
</tpl:layout>
