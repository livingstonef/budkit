<!DOCTYPE html>
<tpl:layout xmlns="http://www.w3.org/1999/xhtml" xmlns:tpl="http://budkit.org/tpl">
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="${page.description}" />
    <meta name="author" content="${page.author}" />
    <meta name="keywords" content="${page.author}" />
    <link rel="shortcut icon" href="${config|design.theme}/assets/ico/favicon.png" />
    <title><tpl:element type="text" data="page.title">Budkit</tpl:element></title>
    <!-- Maverick core CSS -->
    <link href="${config|design.theme}/assets/css/bootstrap.css" rel="stylesheet" />
    <!--        <link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic' rel='stylesheet' type='text/css' />-->
    <!--     <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js" ></script>-->
    <script data-main="${config|general.path}${config|design.theme}assets/js/main" src="${config|design.theme}/assets/js/require.js"></script>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="${config|design.theme}/assets/js/html5shiv.js"></script>
    <script src="${config|design.theme}/assets/js/respond.min.js"></script>
    <![endif]-->
    <tpl:utility type="head" />
</head>
<body>
<div class="hero">
    <div class="navbar navbar-transparent  navbar-static-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#">BudKit</a>
            </div>
            <div class="navbar-collapse collapse">
                <tpl:condition data="user.isauthenticated" test="isset" value="0">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="/member/profile/login" >Sign In</a></li>
                    </ul>
                </tpl:condition>
                <tpl:condition data="user.isauthenticated" test="isset" value="1">
                    <ul class="nav navbar-nav navbar-right">
                        <li class="dropdown">
                            <a href="/system/dashboard/index">Dashboard</a>
                        </li>
                        <li><a href="/member/profile/logout">Sign Out</a></li>
                    </ul>
                </tpl:condition>
            </div><!--/.navbar-collapse-->
        </div>
    </div>
    <div class="container unit">
        <h1>Collaboration Simplified.</h1>
        <p class="highlight">Networking for creative people and thinkers</p>
        <p><a class="btn btn-danger btn-lg" role="button">Join Now, It's FREE</a></p>
    </div>
</div>
<div class="container marketing">
<div class="marketing-title">
    <h2>How It Works</h2>
    <p>Here is how it could help with your research</p>
</div>
<div class="row featurette">
    <div class="col-md-5">
        <h2 class="featurette-heading">8000+ Lab protocols</h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-6 col-md-offset-1">
        <img class="featurette-image img-responsive" src="" />
    </div>
</div>
<div class="row featurette">
    <div class="col-md-6">
        <img class="featurette-image img-responsive" src="" />
    </div>
    <div class="col-md-5 col-md-offset-1">
        <h2 class="featurette-heading"><i class="fa fa-exchange"></i> TheExchange&trade;</h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
</div>
<div class="row featurette">
    <div class="col-md-5">
        <h2 class="featurette-heading">Project Management</h2>
        <p class="lead">Donec ullamcorper nulla non metus auctor fringilla. Vestibulum id ligula porta felis euismod semper. Praesent commodo cursus magna, vel scelerisque nisl consectetur. Fusce dapibus, tellus ac cursus commodo.</p>
    </div>
    <div class="col-md-6 col-md-offset-1">
        <img class="featurette-image img-responsive" src="" />
    </div>
</div>
<hr class="featurette-divider"/>
<div class="marketing-title">
    <h2>Get more from LabSpot</h2>
    <p>Try our advanced features for 30 days</p>
</div>
<div class="row pricing">
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Student</h3>
            </div>
            <div class="panel-body">
                <div class="the-price">
                    <h1>
                        $10<span class="subscript">/mo</span></h1>
                    <small>1 month FREE trial</small>
                </div>
                <table class="table table-striped">

                    <tr>
                        <td>
                            10 Projects
                        </td>
                    </tr>
                    <tr>
                        <td>
                            100K API Access
                        </td>
                    </tr>
                    <tr>
                        <td>
                            1 GB Storage
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Buy on theExchange&trade;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Weekly Reports
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                <a href="#" class="btn btn-default btn-lg" role="button">Sign Up</a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-success">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Professional</h3>
            </div>
            <div class="panel-body">
                <div class="the-price">
                    <h1>
                        $50<span class="subscript">/mo</span></h1>
                    <small>1 month FREE trial</small>
                </div>
                <table class="table table-striped">

                    <tr>
                        <td>
                            150 Projects
                        </td>
                    </tr>
                    <tr>
                        <td>
                            100K API Access
                        </td>
                    </tr>
                    <tr>
                        <td>
                            5 GB Storage
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Buy/Sell on TheExchange&trade;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Weekly Reports
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                <a href="#" class="btn btn-success btn-lg" role="button">Sign Up</a>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">
                    Ultimate</h3>
            </div>
            <div class="panel-body">
                <div class="the-price">
                    <h1>
                        $100<span class="subscript">/mo</span></h1>
                    <small>1 month FREE trial</small>
                </div>
                <table class="table table-striped">

                    <tr>
                        <td>
                            Unlimited Projects
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Unlimited API Access
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Unlimited Storage
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Custom Cloud Services
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Weekly Reports
                        </td>
                    </tr>
                </table>
            </div>
            <div class="panel-footer">
                <a href="#" class="btn btn-info btn-lg" role="button">Sign Up</a></div>
        </div>
    </div>
</div>
<hr class="featurette-divider">
<div class="testimonial">
    <blockquote>"I think it's time research collaboration and communication  makes it to the 21st century. LabSpot makes one giant leap in the right direction."</blockquote>
    <p class="author">Dr Christopher Green - Immunologist</p>
</div>
<footer class="footer">
    <div class="row">
        <div class="col-md-2">
            <h4 class="logo">Labspot</h4>
        </div>
        <div class="col-md-10">
            <div class="row">
                <div class="col-md-3">
                    <h4>About</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">Fact Sheet</a></li>
                        <li><a href="#">Latest Updates</a></li>
                        <li><a href="#">Our Brand</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Support</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">Our Technology</a></li>
                        <li><a href="#">Research Services</a></li>
                        <li><a href="#">Fact Sheet</a></li>
                        <li><a href="#">Terms &amp; Conditions</a></li>
                        <li><a href="#">Privacy Policy</a></li>
                        <li><a href="#">Contact Us</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Investors</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">Our People</a></li>
                        <li><a href="#">Financial Reports</a></li>
                        <li><a href="#">Corporate News</a></li>
                        <li><a href="#">Reports</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4>Connect</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">Facebook</a></li>
                        <li><a href="#">Twitter</a></li>
                        <li><a href="#">Visit Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <p>&copy; LabSpot 2013. Built with love from Gateshead.</p>
</footer>
</div> <!-- /container -->
</body>
</html>
</tpl:layout>

