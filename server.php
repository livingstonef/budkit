<?php
/*
|------------------------------------------------------------------------------
|   o--o  o   o o-o   o  o o-o-o o-o-o
|   |   | |   | |  \  | /    |     |
|   o--o  |   | |   o oo     |     |
|   |   | |   | |  /  | \    |     |
|   o--o   o-o  o-o   o  o o-o-o   o  ALPHA
|
|------------------------------------------------------------------------------
| NAMESPACE
|------------------------------------------------------------------------------
|
| Library	- All Library Classes
| Application	- All Applicaition Action controllers
| Platform      - The Platform Utitlities
|
*/
namespace Platform;

use Application;
use Library;
use Platform;
/*
|---------------------------------------------------------------
| PHP ERROR REPORTING LEVEL
|---------------------------------------------------------------
|
| By default error reporting is set to ALL.  For security
| reasons you are encouraged to change this when your site goes live.
| For more info visit:  http://www.php.net/error_reporting. E Strict became
| Part of E_ALL as of PHP 5.4.0 so use E_ALL except strict standards
|
*/
error_reporting( E_ERROR | E_WARNING | E_PARSE | E_NOTICE );
//xdebug_stop_code_coverage();

/*
|---------------------------------------------------------------
| DEFINE APPLICATION CONSTANTS
|---------------------------------------------------------------
|
| EXT		- The file extension.  Typically ".php"
| SELF		- The name of THIS file (typically "index.php")
| FSPATH	- The full server path to THIS file
| APPPATH	- The full server path to the "application" folder
| DS            - The directory seperator constant
|
*/

define('DS',        DIRECTORY_SEPARATOR);
define('EXT',       '.php');
define('SELF',      pathinfo(__FILE__, PATHINFO_BASENAME));
define('FSPATH',    str_replace(SELF, '', __FILE__)).DS;
define('APPPATH',   FSPATH.'applications'.DS );
define('VENDORPATH',   FSPATH.'vendors'.DS );

/*
|---------------------------------------------------------------
| CONFIG & SHARED ELEMENTS
|---------------------------------------------------------------
|
| All the Base configuration elements are defined in these files.
| You should technically not edit any of this files, application specific
| settings should be added to the config directory of the application in
| question
|
*/
require_once ( FSPATH .'framework'.DS.'utilities'.DS.'defines'.EXT );

/*
 * ------------------------------------------------------
 *  AutoLoad
 * ------------------------------------------------------
 */
require( FSPATH . 'framework' . DS .'utilities'.DS. 'loader' . EXT);
require( FSPATH . 'framework' . DS .'utilities'.DS. 'functions' . EXT);


spl_autoload_register(new Platform\Loader("Platform", array( FSPATH . 'framework'.DS.'utilities', FSPATH . 'vendors'.DS.'utilities' ) ));
spl_autoload_register(new Platform\Loader("Library", array( FSPATH . 'framework' . DS . 'libraries', FSPATH . 'vendors' . DS . 'libraries') ) );
spl_autoload_register(new Platform\Loader("Application", array( FSPATH . 'applications', FSPATH .'vendors'.DS. 'applications')));


/*
 * ------------------------------------------------------
 *  Load all system hooks
 * ------------------------------------------------------
 */
Library\Event::loadHooks();

/*
 * ------------------------------------------------------
 *  Websocket
 * ------------------------------------------------------
 */
$server = new Platform\Protocol\Ws\Server('ws://0.0.0.0:8080/',
    array(
        'allowed_origins' => array(
            'mb.local'
        ),
// Optional defaults:
//     'check_origin'               => true,
//     'connection_manager_class'   => 'ws\ConnectionManager',
//     'connection_manager_options' => array(
//         'timeout_select'           => 0,
//         'timeout_select_microsec'  => 200000,
//         'socket_master_class'      => 'ws\Socket\ServerSocket',
//         'socket_master_options'    => array(
//             'backlog'                => 50,
//             'ssl_cert_file'          => null,
//             'ssl_passphrase'         => null,
//             'ssl_allow_self_signed'  => false,
//             'timeout_accept'         => 5,
//             'timeout_socket'         => 5,
//         ),
//         'connection_class'         => 'ws\Connection',
//         'connection_options'       => array(
//             'socket_class'           => 'ws\Socket\ServerClientSocket',
//             'socket_options'         => array(),
//             'connection_id_secret'   => 'asu5gj656h64Da(0crt8pud%^WAYWW$u76dwb',
//             'connection_id_algo'     => 'sha512'
//         )
//     )
    ));
Library\Event::trigger('onBeforeWebsocketStart', $server);
//Uncomment the following line to test!
//$server->registerApplication('echo', new Platform\Protocol\Ws\Application\Reverb());
$server->run();
